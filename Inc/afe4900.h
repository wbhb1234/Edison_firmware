/**
  ******************************************************************************
  * @file    afe4900.h  
	* @author  ruiyan
	* @date 20160816
  * @version v1.0	
	* @brief   afe4900 API file.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 Huami</center></h2>
  *
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AFE4900_H
#define __AFE4900_H

#ifdef __cplusplus
 extern "C" {
#endif
	 
/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/* define --------------------------------------------------------------------*/
#define CONTROL0        0x00
#define FIFO_NPHASE      4
#define SAMPLE_SIZE      3
#define REG_SIZE         3
#define DISCARD_DATA_NUM 30
#define FIFO_READY_SIZE  84

#define DATA_NUM_FOR_GAIN_CALIBRATION 50
#define DATA_NUM_FOR_SET_I_LED        20
#define DATA_NUM_FOR_SET_I_OFFDAC     20
#define DATA_NUM_FOR_CALIBRATE_OFFDAC 20
#define DATA_NUM_FOR_THRESHOLD_DETECT 20
#define DATA_NUM_FOR_CALI_ERR         20

#define RESET_THRESHOLD           2000000
#define CHANGE_OFFDAC_THRESHOLD   1700000
#define HALF_SCALE                1048576 

#define SUB_STAGE_01 1
#define SUB_STAGE_02 2
#define SUB_STAGE_03 3
#define SUB_STAGE_04 4

#define SUB_STAGE_MAX 4

#define DISCONNECT 1
#define CONNECT 0

#define START_ADC 1
#define STOP_ADC 0

#define RESET_TIMER 1
#define SET_TIMER 0

#define CODE 1
#define VALUE 0

#define LED_1_AMB_CHANNEL   1
#define LED_2_AMB_CHANNEL   2
#define LED_3_AMB_CHANNEL   3
#define LED_4_AMB_CHANNEL   4
#define NO_AMB_CHANNEL     -1

#define INIT_I_LED 20 //mA
#define INIT_I_PD  (3) //uA
#define INIT_TIA_GAIN R_100_K
#define I_PD_ERROR_PERCENT 0.1
#define I_PD_ERROR (INIT_I_PD*I_PD_ERROR_PERCENT)
#define I_DAC_LSB 0.125

#define AUTO_LED_CURRENT  0x00
#define FIXED_LED_CURRENT 0x01

/* enum ----------------------------------------------------------------------*/
enum AFE_WORKING_STAGE 
{
	STAGE_0_CALIBRATE_GAIN                = 0, 
	STAGE_1_SET_I_LED                     = 1, 
	STAGE_2_SET_I_OFFDAC                  = 2, 
	STAGE_3_CALIBRATE_I_OFFDAC            = 3,
	STAGE_4_CALI_ERR_FROME_I_LED_CHANGE   = 4,	
	STAGE_5_POLL_AND_THRESHOLD_DETECT     = 5,
};

enum TIA_GAIN_CODE 
{
	R_500_K  = 0, 
	R_250_K  = 1, 
	R_100_K  = 2, 
	R_50_K   = 3,
	R_25_K   = 4,
	R_10_K   = 5,
	R_1000_K = 6, 
	R_2000_K = 7,
	R_1500_K = 8,
};

/* struct --------------------------------------------------------------------*/
struct afe4900_ppg_sensor_data_t
{
	int32_t  channel_1234_data_sum[4];  //the summation of ppg data
	uint8_t  num_of_data_sum;           //the number be used to caculate the summation of ppg data
	uint8_t  num_of_data_total;         //the number be of ppg data
	int32_t  channel_1234_data_max[4];
	int32_t  channel_1234_data_min[4];
};

struct afe4900_ppg_sensor_config_t
{
	int32_t             LED_2314_current[4];
	int32_t             LED_2314_cur_code[4];
	int32_t             LED_2314_cur_fixed[4];
	double              LED_2314_DAC[4];
	int32_t             LED_2314_DAC_CALI[4];
	enum TIA_GAIN_CODE  LED_2314_gain_code[4];	
};

struct afe4900_ppg_sensor_calibration_t //y=kx+b
{
	double  LED_1234_k[4];
	int32_t LED_1234_b[4];
	double  mean_k;
	int32_t mean_b;
	int8_t  calibrated;
	int32_t buf_point[4][4];
};

struct afe4900_ppg_sensor_t
{
	struct  afe4900_ppg_sensor_data_t         sensor_data;
	struct  afe4900_ppg_sensor_config_t       sensor_config;
	struct  afe4900_ppg_sensor_calibration_t  sensor_calibration;
	enum    AFE_WORKING_STAGE                 afe_working_stage_x;
	uint8_t                                   sub_stage;
	void    (*SPI_write_data_p)        (struct afe4900_ppg_sensor_t *sensor_p, uint8_t Reg_address, uint8_t *pData, uint16_t Size);
	void    (*SPI_read_data_p)         (struct afe4900_ppg_sensor_t *sensor_p, uint8_t Reg_address,uint8_t *pData, uint16_t Size);
	void    (*set_afe_resetz_pin_high_p)   (struct afe4900_ppg_sensor_t *sensor_p);
	void    (*set_afe_resetz_pin_low_p)    (struct afe4900_ppg_sensor_t *sensor_p);
	void    (*set_afe_spi_nss_pin_high_p) (struct afe4900_ppg_sensor_t *sensor_p);
	void    (*set_afe_spi_nss_pin_low_p)  (struct afe4900_ppg_sensor_t *sensor_p);
	void    (*disable_afe_interrupt_p) (struct afe4900_ppg_sensor_t *sensor_p);
	void    (*enable_afe_interrupt_p)  (struct afe4900_ppg_sensor_t *sensor_p);
	int8_t amb_channel_ledx;
	int8_t auto_led_current_or_not;
};

/****************************************************************/
/* Global functions												*/
/****************************************************************/

void AFE4900_Init(struct afe4900_ppg_sensor_t *sensor_p);
void start_stop_ADC(struct afe4900_ppg_sensor_t *sensor_p, uint8_t start);
void delay_us(uint32_t nus);
int16_t afe4900_read_ppg_data(struct afe4900_ppg_sensor_t *sensor_p,int32_t data_buf[32][4]);
void AFE4900_Enable_Read (struct afe4900_ppg_sensor_t *sensor_p);
signed long AFE4900_Reg_Read(struct afe4900_ppg_sensor_t *sensor_p, unsigned char Reg_address);
void AFE4900_Disable_Read (struct afe4900_ppg_sensor_t *sensor_p);
void AFE4900_Reg_Write (struct afe4900_ppg_sensor_t *sensor_p, unsigned char reg_address, unsigned long data);
	 
	 
#ifdef __cplusplus
}
#endif

#endif /*__AFE4900_H*/
