/**
  ******************************************************************************
  * @file    ppg_sensor.h 
	* @author  ruiyan
	* @date 20180515
  * @version v1.0	
	* @brief   .
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2018 Huami</center></h2>
  *
  ******************************************************************************
  */ 


#ifndef __PPG_SENSOR_H
#define __PPG_SENSOR_H

#ifdef __cplusplus
 extern "C" {
#endif

/*************************************************************************************************************************/
#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_spi.h"
#include "stdint.h"	
#include "afe4900.h"
	 
extern struct afe4900_ppg_sensor_t PPG_sensor_01;
extern struct afe4900_ppg_sensor_t PPG_sensor_02;
extern struct afe4900_ppg_sensor_t PPG_sensor_03;

/* AFE01 02 03***********************************************************************************************************/
void AFE01_02_03_SPI_write_data(struct afe4900_ppg_sensor_t *sensor_p, uint8_t Reg_address, uint8_t *pData, uint16_t Size);
void AFE01_02_03_SPI_read_data(struct afe4900_ppg_sensor_t *sensor_p, uint8_t Reg_address,uint8_t *pData, uint16_t Size);

/* AFE01 ****************************************************************************************************************/
void AFE01_set_afe_resetz_pin_high(struct afe4900_ppg_sensor_t *sensor_p);
void AFE01_set_afe_resetz_pin_low(struct afe4900_ppg_sensor_t *sensor_p);
void AFE01_set_afe_spi1_nss_pin_high(struct afe4900_ppg_sensor_t *sensor_p);
void AFE01_set_afe_spi1_nss_pin_low(struct afe4900_ppg_sensor_t *sensor_p);
void AFE01_disable_afe_interrupt(struct afe4900_ppg_sensor_t *sensor_p);
void AFE01_enable_afe_interrupt(struct afe4900_ppg_sensor_t *sensor_p);

/* AFE02 ****************************************************************************************************************/
void AFE02_set_afe_resetz_pin_high(struct afe4900_ppg_sensor_t *sensor_p);
void AFE02_set_afe_resetz_pin_low(struct afe4900_ppg_sensor_t *sensor_p);
void AFE02_set_afe_spi1_nss_pin_high(struct afe4900_ppg_sensor_t *sensor_p);
void AFE02_set_afe_spi1_nss_pin_low(struct afe4900_ppg_sensor_t *sensor_p);
void AFE02_disable_afe_interrupt(struct afe4900_ppg_sensor_t *sensor_p);
void AFE02_enable_afe_interrupt(struct afe4900_ppg_sensor_t *sensor_p);

/* AFE03 ****************************************************************************************************************/
void AFE03_set_afe_resetz_pin_high(struct afe4900_ppg_sensor_t *sensor_p);
void AFE03_set_afe_resetz_pin_low(struct afe4900_ppg_sensor_t *sensor_p);
void AFE03_set_afe_spi1_nss_pin_high(struct afe4900_ppg_sensor_t *sensor_p);
void AFE03_set_afe_spi1_nss_pin_low(struct afe4900_ppg_sensor_t *sensor_p);
void AFE03_disable_afe_interrupt(struct afe4900_ppg_sensor_t *sensor_p);
void AFE03_enable_afe_interrupt(struct afe4900_ppg_sensor_t *sensor_p); 	 

#ifdef __cplusplus
}
#endif

#endif /*__PPG_SENSOR_H*/
