/**
  ******************************************************************************
  * @file    nucleol476Port.h 
	* @author  ruiyan
	* @date 20180515
  * @version v1.0	
	* @brief   .
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2018 Huami</center></h2>
  *
  ******************************************************************************
  */ 


#ifndef __NUCLEOL476PORT_H
#define __NUCLEOL476PORT_H

#ifdef __cplusplus
 extern "C" {
#endif

	 uint32_t AD5940_GetMCUIntFlag(void);
	 uint32_t AD5940_ClrMCUIntFlag(void);
	 void AD5940_Enable_Interrupt(void);
	 void AD5940_Disable_Interrupt(void);
	 uint32_t AD5940_MCUResourceInit(void *pCfg);
	 void AD5940_RstClr(void);
	 void AD5940_RstSet(void);
	 void AD5940_CsSet(void);
	void AD5940_Delay10us(uint32_t time);
	 
	 
#ifdef __cplusplus
}
#endif

#endif /*__NUCLEOL476PORT_H*/
