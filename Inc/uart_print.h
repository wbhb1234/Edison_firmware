/**
  ******************************************************************************
  * @file    uart_print.h  
	* @author  ruiyan
	* @date 20180504
  * @version v1.0	
	* @brief   .
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2018 Huami</center></h2>
  *
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion **************************************/
#ifndef __UART_PRINT_H
#define __UART_PRINT_H

#ifdef __cplusplus
 extern "C" {
#endif

#include "stdint.h"
	 
#define TIMEOUT_UART_PRINT 0xffffffff
void print_s22bit(int32_t value);	 
void print_rn(void);
void print_sp(void);
	 
#ifdef __cplusplus
}
#endif

#endif /*__UART_PRINT_H*/
