/*!
*****************************************************************************
@file:    NUCLEOL476Port.c
@author:  $Author: ADI $
@brief:   The port for ST's Nucleo L452 board.
@version: $Revision: 1 $
@date:    $Date: 2018-04-16 14:09:35 +0100 (Mon, 16 Apr 2018) $
-----------------------------------------------------------------------------
Copyright (c) 2012-2017 Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Modified versions of the software must be conspicuously marked as such.
  - This software is licensed solely and exclusively for use with processors
    manufacTRUEd by or for Analog Devices, Inc.
  - This software may not be combined or merged with other code in any manner
    that would cause the software to become subject to terms and conditions
    which differ from those listed here.
  - Neither the name of Analog Devices, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
  - The use of this software may or may not infringe the patent rights of one
    or more patent holders.  This license does not release you from the
    requirement that you obtain separate licenses from these patent holders
    to use this software.
THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR ECGLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-
INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL ANALOG DEVICES, INC. OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF
CLAIMS OF INTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

#include <AD5940.h>
#include "stdio.h"
#include "stm32l4xx_hal.h"
#include "spi.h"

/* Definition for STM32 SPI clock resources */
#define AD5940SPI                          SPI2
#define AD5940_CLK_ENABLE()                __HAL_RCC_SPI2_CLK_ENABLE()
#define AD5940_SCK_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOB_CLK_ENABLE()
#define AD5940_MISO_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOB_CLK_ENABLE()
#define AD5940_MOSI_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOB_CLK_ENABLE()

#define AD5940_SCK_PIN                     GPIO_PIN_13
#define AD5940_SCK_GPIO_PORT               GPIOB
#define AD5940_SCK_AF                      GPIO_AF5_SPI2
#define AD5940_MISO_PIN                    GPIO_PIN_14
#define AD5940_MISO_GPIO_PORT              GPIOB
#define AD5940_MISO_AF                     GPIO_AF5_SPI2
#define AD5940_MOSI_PIN                    GPIO_PIN_15
#define AD5940_MOSI_GPIO_PORT              GPIOB
#define AD5940_MOSI_AF                     GPIO_AF5_SPI2

#define AD5940SPI_FORCE_RESET()               __HAL_RCC_SPI2_FORCE_RESET()
#define AD5940SPI_RELEASE_RESET()             __HAL_RCC_SPI2_RELEASE_RESET()

/* Definition for AD5940_01 Pins */
#define AD5940_01_CS_GPIO_CLK_ENABLE()        __HAL_RCC_GPIOB_CLK_ENABLE()
#define AD5940_01_RST_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOB_CLK_ENABLE()
#define AD5940_01_GP0INT_GPIO_CLK_ENABLE()    __HAL_RCC_GPIOB_CLK_ENABLE()

#define AD5940_01_CS_PIN                      GPIO_PIN_5   //ICG1
#define AD5940_01_CS_GPIO_PORT                GPIOB

#define AD5940_01_RST_PIN                     GPIO_PIN_9   //ICG1
#define AD5940_01_RST_GPIO_PORT               GPIOB

#define AD5940_01_GP0INT_PIN                  GPIO_PIN_8  // ICG1  
#define AD5940_01_GP0INT_GPIO_PORT            GPIOB
#define AD5940_01_GP0INT_IRQn                 EXTI9_5_IRQn

/* Definition for AD5940_02 Pins */
#define AD5940_02_CS_GPIO_CLK_ENABLE()        __HAL_RCC_GPIOB_CLK_ENABLE()
#define AD5940_02_RST_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOC_CLK_ENABLE()
#define AD5940_02_GP0INT_GPIO_CLK_ENABLE()    __HAL_RCC_GPIOC_CLK_ENABLE()

#define AD5940_02_CS_PIN                      GPIO_PIN_12   //ICG2
#define AD5940_02_CS_GPIO_PORT                GPIOB

#define AD5940_02_RST_PIN                     GPIO_PIN_13   //ICG2
#define AD5940_02_RST_GPIO_PORT               GPIOC

#define AD5940_02_GP0INT_PIN                  GPIO_PIN_9  // ICG2  
#define AD5940_02_GP0INT_GPIO_PORT            GPIOC
#define AD5940_02_GP0INT_IRQn                 EXTI9_5_IRQn

#define SYSTICK_MAXCOUNT ((1L<<24)-1)   /* we use Systick to complete function Delay10uS(). This value only applies to NUCLEOL452 board. */
#define SYSTICK_CLKFREQ   80000000L     /* Systick clock frequency in Hz. This only appies to NUCLEOL452 board */

SPI_HandleTypeDef  SpiHandle;

volatile static uint32_t TmrIntCount = 0; /* Used for timer */
uint8_t S01_ucInterrupted = 0;       /* Flag to indicate interrupt occurred */
uint8_t S02_ucInterrupted = 0;       /* Flag to indicate interrupt occurred */
uint8_t Sensor_slt=1;


/**
@brief void SpiReadWriteNBytes(unsigned char *pSendBuffer,unsigned char *pRecvBuff,unsigned long length)  
========== Using SPI to transmit N bytes and return the received bytes. This function is target to 
provide a more efficent way to transmit/receive data.
@param pSendBuffer :{0 - 0xFFFFFFFF}
- Pointer to the data to be sent.
@param pRecvBuff :{0 - 0xFFFFFFFF}
- Pointer to the buffer used to store received data.
@param length :{0 - 0xFFFFFFFF}
- Data length in SendBuffer.
@return None.
**/
void AD5940_ReadWriteNBytes(unsigned char *pSendBuffer,unsigned char *pRecvBuff,unsigned long length)
{
  HAL_SPI_TransmitReceive(&hspi2, pSendBuffer, pRecvBuff, length, (uint32_t)-1);
}

void AD5940_CsClr(void)
{
	if(Sensor_slt)
  HAL_GPIO_WritePin(AD5940_01_CS_GPIO_PORT, AD5940_01_CS_PIN, GPIO_PIN_RESET);
	else
	HAL_GPIO_WritePin(AD5940_02_CS_GPIO_PORT, AD5940_02_CS_PIN, GPIO_PIN_RESET);
}

void AD5940_CsSet(void)
{
	if(Sensor_slt)
  HAL_GPIO_WritePin(AD5940_01_CS_GPIO_PORT, AD5940_01_CS_PIN, GPIO_PIN_SET);
	else
	HAL_GPIO_WritePin(AD5940_02_CS_GPIO_PORT, AD5940_02_CS_PIN, GPIO_PIN_SET);
}

/* Pull high AD5940 Reset Pin */
void AD5940_RstSet(void)
{
	if(Sensor_slt)
  HAL_GPIO_WritePin(AD5940_01_RST_GPIO_PORT, AD5940_01_RST_PIN, GPIO_PIN_SET);
	else
	HAL_GPIO_WritePin(AD5940_02_RST_GPIO_PORT, AD5940_02_RST_PIN, GPIO_PIN_SET);
}

void AD5940_RstClr(void)
{
	if(Sensor_slt)
  HAL_GPIO_WritePin(AD5940_01_RST_GPIO_PORT, AD5940_01_RST_PIN, GPIO_PIN_RESET);
	else
	HAL_GPIO_WritePin(AD5940_02_RST_GPIO_PORT, AD5940_02_RST_PIN, GPIO_PIN_RESET);
}

void AD5940_Delay10us(uint32_t time)
{
	/*
  if(time==0)return;
  if(time*10<SYSTICK_MAXCOUNT/(SYSTICK_CLKFREQ/1000000)){
    SysTick->LOAD = time*10*(SYSTICK_CLKFREQ/1000000);
    SysTick->VAL = 0;
    SysTick->CTRL = (1 << 2) | (1<<0);    
    while(((SysTick->CTRL)&(1<<16))==0);    
    SysTick->CTRL = 0;                    
  }
  else {
    AD5940_Delay10us(time/2);
    AD5940_Delay10us(time/2 + (time&1));
  }
	*/
	
	time = time*200;
		do	{
		time--;
	}while(time);
	
}


uint32_t AD5940_01_GetMCUIntFlag(void)
{
  return S01_ucInterrupted;
}

uint32_t AD5940_01_ClrMCUIntFlag(void)
{
  S01_ucInterrupted = 0;
  return 1;
}

uint32_t AD5940_02_GetMCUIntFlag(void)
{
  return S02_ucInterrupted;
}

uint32_t AD5940_02_ClrMCUIntFlag(void)
{
  S02_ucInterrupted = 0;
  return 1;
}


void AD5940_Enable_Interrupt(void)
{	
	if(Sensor_slt)
	HAL_NVIC_EnableIRQ(AD5940_01_GP0INT_IRQn);
	else
	HAL_NVIC_EnableIRQ(AD5940_02_GP0INT_IRQn);
}

void AD5940_Disable_Interrupt(void)					
{
	if(Sensor_slt)
	HAL_NVIC_DisableIRQ(AD5940_01_GP0INT_IRQn);
	else
	HAL_NVIC_DisableIRQ(AD5940_02_GP0INT_IRQn);
}
uint32_t AD5940_MCUResourceInit(void *pCfg)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /* Step1, initialize SPI peripheral and its GPIOs for CS/RST */
  AD5940_SCK_GPIO_CLK_ENABLE();
  AD5940_MISO_GPIO_CLK_ENABLE();
  AD5940_MOSI_GPIO_CLK_ENABLE();
	/* Enable SPI clock */
	AD5940_CLK_ENABLE(); 
		
	GPIO_InitStruct.Pin       = AD5940_SCK_PIN;
	GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull      = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = AD5940_SCK_AF;
	HAL_GPIO_Init(AD5940_SCK_GPIO_PORT, &GPIO_InitStruct);
		
	/* SPI MISO GPIO pin configuration  */
	GPIO_InitStruct.Pin = AD5940_MISO_PIN;
	GPIO_InitStruct.Alternate = AD5940_MISO_AF;
	HAL_GPIO_Init(AD5940_MISO_GPIO_PORT, &GPIO_InitStruct);
		
	/* SPI MOSI GPIO pin configuration  */
	GPIO_InitStruct.Pin = AD5940_MOSI_PIN;
	GPIO_InitStruct.Alternate = AD5940_MOSI_AF;
	HAL_GPIO_Init(AD5940_MOSI_GPIO_PORT, &GPIO_InitStruct);
	/* Set the SPI parameters */
	SpiHandle.Instance               = AD5940SPI;
	SpiHandle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
	SpiHandle.Init.Direction         = SPI_DIRECTION_2LINES;
	SpiHandle.Init.CLKPhase          = SPI_PHASE_1EDGE;
	SpiHandle.Init.CLKPolarity       = SPI_POLARITY_LOW;
	SpiHandle.Init.DataSize          = SPI_DATASIZE_8BIT;
	SpiHandle.Init.FirstBit          = SPI_FIRSTBIT_MSB;
	SpiHandle.Init.TIMode            = SPI_TIMODE_DISABLE;
	SpiHandle.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;
	SpiHandle.Init.CRCPolynomial     = 7;
	SpiHandle.Init.NSS               = SPI_NSS_SOFT;
	SpiHandle.Init.Mode = SPI_MODE_MASTER;
	HAL_SPI_Init(&SpiHandle);	
	
	if(Sensor_slt)
	{
		AD5940_01_CS_GPIO_CLK_ENABLE();
		AD5940_01_RST_GPIO_CLK_ENABLE();
		/* SPI CS GPIO pin configuration  */
		GPIO_InitStruct.Pin = AD5940_01_CS_PIN;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		HAL_GPIO_Init(AD5940_01_CS_GPIO_PORT, &GPIO_InitStruct);
		
		/* SPI RST GPIO pin configuration  */
		GPIO_InitStruct.Pin = AD5940_01_RST_PIN;
		HAL_GPIO_Init(AD5940_01_RST_GPIO_PORT, &GPIO_InitStruct);
		
		AD5940_CsSet();
		AD5940_RstSet();
	
		/* Step 2: Configure external interrupot line */
		AD5940_01_GP0INT_GPIO_CLK_ENABLE();
		GPIO_InitStruct.Pin       = AD5940_01_GP0INT_PIN;
		GPIO_InitStruct.Mode      = GPIO_MODE_IT_FALLING;
		GPIO_InitStruct.Pull      = GPIO_PULLUP;
		GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = 0;
		HAL_GPIO_Init(AD5940_01_GP0INT_GPIO_PORT, &GPIO_InitStruct);
		
		/* Enable and set EXTI Line0 Interrupt to the lowest priority */
		HAL_NVIC_EnableIRQ(AD5940_01_GP0INT_IRQn);
		HAL_NVIC_SetPriority(AD5940_01_GP0INT_IRQn, 0, 0);
	}
	else
	{
		AD5940_02_CS_GPIO_CLK_ENABLE();
		AD5940_02_RST_GPIO_CLK_ENABLE();
		/* Enable SPI clock */
	
		/* SPI CS GPIO pin configuration  */
		GPIO_InitStruct.Pin = AD5940_02_CS_PIN;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		HAL_GPIO_Init(AD5940_02_CS_GPIO_PORT, &GPIO_InitStruct);
		
		/* SPI RST GPIO pin configuration  */
		GPIO_InitStruct.Pin = AD5940_02_RST_PIN;
		HAL_GPIO_Init(AD5940_02_RST_GPIO_PORT, &GPIO_InitStruct);
		
		AD5940_CsSet();
		AD5940_RstSet();
		
		/* Step 2: Configure external interrupot line */
		AD5940_02_GP0INT_GPIO_CLK_ENABLE();
		GPIO_InitStruct.Pin       = AD5940_02_GP0INT_PIN;
		GPIO_InitStruct.Mode      = GPIO_MODE_IT_FALLING;
		GPIO_InitStruct.Pull      = GPIO_PULLUP;
		GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = 0;
		HAL_GPIO_Init(AD5940_02_GP0INT_GPIO_PORT, &GPIO_InitStruct);
		
		/* Enable and set EXTI Line0 Interrupt to the lowest priority */
		HAL_NVIC_EnableIRQ(AD5940_02_GP0INT_IRQn);
		HAL_NVIC_SetPriority(AD5940_02_GP0INT_IRQn, 0, 0);
	}
	
  return 0;
}
/**
  * @brief EXTI line detection callbacks
  * @param GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */
/*
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  if (GPIO_Pin == AD5940_GP0INT_PIN)
  {
		ucInterrupted=1;
  }
}
*/





