/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "AD5940.H"
#include "string.h"
#include <stdlib.h>
#include "BodyImpedance.h"
#include "uart_print.h"
#include "afe4900.h"
#include "ppg_sensor.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
#define ECG_SAMPLE_NUM 21
uint32_t ECG_AD_Value[ECG_SAMPLE_NUM];
uint8_t fifo_ready_flag = FIFO_NOT_READY;
uint8_t new_command_received_flag = NO_NEW_COMMAND;
uint8_t uart_buffer[6] = {0,0,0,0,0,0};
	uint32_t mydata = 0;
	int32_t data_buf01[32][4];
	int32_t data_buf02[32][4];
	int32_t data_buf03[32][4];

#define APPBUFF_SIZE 512
uint32_t App_01_Buff[APPBUFF_SIZE];
uint32_t App_02_Buff[APPBUFF_SIZE];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void Read_And_Send_Sensor_Data(int32_t buf01[32][4],int32_t buf02[32][4],int32_t buf03[32][4]);
void Sensor_Init(void);
void UART_command_check_and_execute(void);
void fixed_led_current_set(void);
	
int32_t BIAShowResult(uint32_t *pData, uint32_t DataCount);
static int32_t AD5940PlatformCfg(void);
void AD5940BIAStructInit(void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  uint32_t temp;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();
  MX_ADC1_Init();
  MX_SPI2_Init();
  /* USER CODE BEGIN 2 */
	/* ADC Start!~ */
	//printf("Init ing...!~\r\n");
	/* ### - 1 - Start calibration ############################################ */
  if (HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED) !=  HAL_OK)
  {
    Error_Handler();
  }
	/* ### - 2 - Start conversion in DMA mode ################################# */

	/* ### - 3 - Start PPG in cont mode ################################# */
	//Sensor_Init();

	/* ### - 4 - Start ICG1 in cont mode ################################# */
	
	Sensor_slt = 1;
	
	AD5940_MCUResourceInit(0);
  AD5940PlatformCfg(); 
  AD5940BIAStructInit(); /* Configure your parameters in this function */
  AppBIAInit(App_01_Buff, APPBUFF_SIZE);    /* Initialize BIA application. Provide a buffer, which is used to store sequencer commands */
	
	Sensor_slt = 0;
	
	AD5940_MCUResourceInit(0);
  AD5940PlatformCfg(); 
  AD5940BIAStructInit(); /* Configure your parameters in this function */
  AppBIAInit(App_02_Buff, APPBUFF_SIZE);    /* Initialize BIA application. Provide a buffer, which is used to store sequencer commands */

  //HAL_UART_Receive_IT(&huart1,uart_buffer,6);	
	
  //if (HAL_ADC_Start_DMA(&hadc1, (uint32_t*)&ECG_AD_Value, ECG_SAMPLE_NUM) != HAL_OK)
  {
    //Error_Handler();
  }
	//printf("ALL Init OK!~\r\n");
	
	//Sensor_slt = 0;
	
  //AppBIACtrl(BIACTRL_SHUTDOWN, 0);         /* Control BIA measurment to start. Second parameter has no meaning with this command. */
	
	//Sensor_slt = 1;
	//AppBIAInit(0, 0); 
	AppBIACtrl(BIACTRL_START, 0);
	HAL_Delay(10);
	Sensor_slt = 1;
	AppBIAInit(0, 0); 
	AppBIACtrl(BIACTRL_START, 0);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */		
		if(AD5940_02_GetMCUIntFlag())
		{
			AD5940_02_ClrMCUIntFlag(); /* Clear this flag */
			Sensor_slt = 0;
			temp = APPBUFF_SIZE;
      AppBIAISR(App_02_Buff, &temp); /* Deal with it and provide a buffer to store data we got */
			//AppBIACtrl(BIACTRL_SHUTDOWN,0);
			//BIAShowResult(App_02_Buff,temp);
			
			//Sensor_slt = 1;
			//AppBIAInit(0, 0); 
			//AppBIACtrl(BIACTRL_START, 0);
		}
		
		if(AD5940_01_GetMCUIntFlag())
		{
			AD5940_01_ClrMCUIntFlag(); /* Clear this flag */
			Sensor_slt = 1;
      temp = APPBUFF_SIZE;
      AppBIAISR(App_01_Buff, &temp); /* Deal with it and provide a buffer to store data we got */
			//AppBIACtrl(BIACTRL_SHUTDOWN,0);
			//发送数据
			//printf("A");
			
			BIAShowResult(App_02_Buff,temp);
			BIAShowResult(App_01_Buff,temp);
			printf("\r\n");
			
			//Sensor_slt = 0;
			//AppBIAInit(0, 0); 
			//AppBIACtrl(BIACTRL_START, 0);
		}
		
		//printf("Test Init OK!~\r\n");
		//HAL_Delay(500);
		/*
		if(fifo_ready_flag == FIFO_READY)
		{
			Read_And_Send_Sensor_Data(data_buf01,data_buf02,data_buf03);
			//PPG_sensor_01.disable_afe_interrupt_p(&PPG_sensor_01);
			//AD5940_Enable_Interrupt();
		}
		if(new_command_received_flag == NEW_COMMAND_RECEIVED)
		{
			UART_command_check_and_execute();			
		}		
		*/
		
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 40;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_ADC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_MSI;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 20;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  HAL_RCCEx_EnableLSCO(RCC_LSCOSOURCE_LSI);
  /**Configure the main internal regulator output voltage 
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART */
 
  HAL_UART_Transmit(&huart1,(uint8_t *)&ch,1,1000);
  return ch;
}
/**
  * @brief EXTI line detection callbacks
  * @param GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) 
{
	switch(GPIO_Pin)
		{
			case GPIO_PIN_2:  //PPG FIFO INT
				  fifo_ready_flag = FIFO_READY;
			    PPG_sensor_01.disable_afe_interrupt_p(&PPG_sensor_01);
			break;
			case GPIO_PIN_8: //ICG_01 FIFO INT
					S01_ucInterrupted=1;
			break;
			case GPIO_PIN_9: //ICG_02 FIFO INT
					S02_ucInterrupted=1;
			break;
			default:break;
		} 
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	new_command_received_flag = NEW_COMMAND_RECEIVED; 
}

static void Sensor_Init(void)
{
	AFE4900_Init(&PPG_sensor_01);
	//AFE4900_Init(&PPG_sensor_02);
	//AFE4900_Init(&PPG_sensor_03);	
	
	start_stop_ADC(&PPG_sensor_01, START_ADC); 
	//delay_us(29840);                             // ~3ms delay  with 32MHz clock
	//start_stop_ADC(&PPG_sensor_02, START_ADC);
	//delay_us(29840);                             // ~3ms delay  with 32MHz clock
	//start_stop_ADC(&PPG_sensor_03, START_ADC);
	
	//先关中断 AD5940 FIFO读完后 再开启
	PPG_sensor_01.enable_afe_interrupt_p(&PPG_sensor_01);
	fifo_ready_flag = FIFO_NOT_READY;
}
uint32_t gLastTick = 0;
uint32_t gTickInterval = 0;
uint32_t gSampleRate = 0;
uint16_t gNibpSample[100] = {0};
static void Read_And_Send_Sensor_Data(int32_t buf01[32][4],int32_t buf02[32][4],int32_t buf03[32][4])
{
	uint8_t data_size = 0;
	//uint8_t tNibpSize = 0;
	            //afe4900_read_ppg_data(&PPG_sensor_01,buf01);
	            //afe4900_read_ppg_data(&PPG_sensor_02,buf02);
	data_size = afe4900_read_ppg_data(&PPG_sensor_01,buf01);
	//tNibpSize = ReadNibpAdcValue(gNibpSample);
	fifo_ready_flag = FIFO_NOT_READY;
	PPG_sensor_01.enable_afe_interrupt_p(&PPG_sensor_01);
	/*
	if(gLastTick != 0)
	{
		gTickInterval = HAL_GetTick()-gLastTick;
		gSampleRate = 1000.0*tNibpSize / gTickInterval+0.5;
	}
	gLastTick = HAL_GetTick();
	*/
	for(int i=0;i<data_size;i++ )
	{
		print_sp();
				
		print_s22bit(buf01[i][0]);print_sp();
		print_s22bit(buf01[i][1]);print_sp();
		print_s22bit(buf01[i][2]);print_sp();
		print_s22bit(buf01[i][3]);print_sp();		
		//delay_us(29840/6);                             // ~3ms delay  with 32MHz clock	
		
		print_s22bit(ECG_AD_Value[i]);print_sp();
		//print_s22bit(buf02[i][0]);print_sp();           //
		print_s22bit(buf02[i][1]);print_sp();
		print_s22bit(buf02[i][2]);print_sp();
		print_s22bit(buf02[i][3]);print_sp();
		//delay_us(29840/6);                             // ~3ms delay  with 32MHz clock	
		
		print_s22bit(buf03[i][0]);print_sp();
		print_s22bit(buf03[i][1]);print_sp();
		print_s22bit(buf03[i][2]);print_sp();
		print_s22bit(buf03[i][3]);print_sp();
		//delay_us(29840/6);                             // ~3ms delay  with 32MHz clock
		
		print_rn();
 		//delay_us(29840);                             // ~3ms delay  with 32MHz clock
		delay_us(29840);                             // ~3ms delay  with 32MHz clock
	} 
}

void UART_command_check_and_execute(void)
{	
	if(uart_buffer[0] == COMMAND_PACKAGE_BEGIN && uart_buffer[5] == COMMAND_PACKAGE_END)
	{
		switch(uart_buffer[4])	
		{
			case (AUTO_LED_CURRENT) :
			case (FIXED_LED_CURRENT):
			{	
				start_stop_ADC(&PPG_sensor_01, STOP_ADC); 
				//start_stop_ADC(&PPG_sensor_02, STOP_ADC);
				//start_stop_ADC(&PPG_sensor_03, STOP_ADC);
				PPG_sensor_01.auto_led_current_or_not = uart_buffer[4];
				//PPG_sensor_02.auto_led_current_or_not = uart_buffer[4];
				//PPG_sensor_03.auto_led_current_or_not = uart_buffer[4];				
				fixed_led_current_set();
				Sensor_Init();	
			} break;
		}				 
	}
	else if(uart_buffer[0] == REG_DATA_PACKAGE_BEGIN && uart_buffer[5] == REG_DATA_PACKAGE_END)
	{
		//SPI_write_data(uart_buffer[1],uart_buffer+2, 3);//write afe4900 reg
	}
	new_command_received_flag = NO_NEW_COMMAND;
	HAL_UART_Receive_IT(&huart1,uart_buffer,6);	
	//PPG_sensor_01.enable_afe_interrupt_p(&PPG_sensor_01);
}

void fixed_led_current_set(void)
{
	PPG_sensor_01.sensor_config.LED_2314_cur_fixed[0] = 0xff;
	PPG_sensor_01.sensor_config.LED_2314_cur_fixed[1] = 0xff;
	PPG_sensor_01.sensor_config.LED_2314_cur_fixed[2] = 0xb7;
	PPG_sensor_01.sensor_config.LED_2314_cur_fixed[3] = 0x00;	
	
	//PPG_sensor_02.sensor_config.LED_2314_cur_fixed[0] = 0x48;
	//PPG_sensor_02.sensor_config.LED_2314_cur_fixed[1] = 0x5f;
	//PPG_sensor_02.sensor_config.LED_2314_cur_fixed[2] = 0xff;
	//PPG_sensor_02.sensor_config.LED_2314_cur_fixed[3] = 0x36;
	
	//PPG_sensor_03.sensor_config.LED_2314_cur_fixed[0] = 0xff;
	//PPG_sensor_03.sensor_config.LED_2314_cur_fixed[1] = 0xff;
	//PPG_sensor_03.sensor_config.LED_2314_cur_fixed[2] = 0xff;
	//PPG_sensor_03.sensor_config.LED_2314_cur_fixed[3] = 0x00;
}

/* It's your choice here how to do with the data. Here is just an example to print them to UART */
int32_t BIAShowResult(uint32_t *pData, uint32_t DataCount)
{
  //float freq;

  fImpPol_Type *pImp = (fImpPol_Type*)pData;
  //AppBIACtrl(BIACTRL_GETFREQ, &freq);

  // printf("Freq:%.2f ", freq);
  /*Process data*/
  for(int i=0;i<DataCount;i++)
  {
    //printf("RzMag: %f Ohm , RzPhase: %f \n",pImp[i].Magnitude,pImp[i].Phase*180/MATH_PI);
		// printf("%0.2f,%0.2f\n",pImp[i].Magnitude,pImp[i].Phase*180/MATH_PI);
		 printf("%0.2f	",pImp[i].Magnitude);
  }
  return 0;
}
/* Initialize AD5940 basic blocks like clock */
static int32_t AD5940PlatformCfg(void)
{
  CLKCfg_Type clk_cfg;
  FIFOCfg_Type fifo_cfg;
  AGPIOCfg_Type gpio_cfg;

  /* Use hardware reset */
  AD5940_HWReset();
  /* Platform configuration */
  AD5940_Initialize();
  /* Step1. Configure clock */
  clk_cfg.ADCClkDiv = ADCCLKDIV_1;
  clk_cfg.ADCCLkSrc = ADCCLKSRC_HFOSC;
  clk_cfg.SysClkDiv = SYSCLKDIV_1;
  clk_cfg.SysClkSrc = SYSCLKSRC_HFOSC;
  clk_cfg.HfOSC32MHzMode = bFALSE;
  clk_cfg.HFOSCEn = bTRUE;
  clk_cfg.HFXTALEn = bFALSE;
  clk_cfg.LFOSCEn = bTRUE;
  AD5940_CLKCfg(&clk_cfg);
  /* Step2. Configure FIFO and Sequencer*/
  fifo_cfg.FIFOEn = bFALSE;
  fifo_cfg.FIFOMode = FIFOMODE_FIFO;
  fifo_cfg.FIFOSize = FIFOSIZE_4KB;                       /* 4kB for FIFO, The reset 2kB for sequencer */
  fifo_cfg.FIFOSrc = FIFOSRC_DFT; // 
  fifo_cfg.FIFOThresh = 4;//AppBIACfg.FifoThresh;        /* DFT result. One pair for RCAL, another for Rz. One DFT result have real part and imaginary part */
  AD5940_FIFOCfg(&fifo_cfg);                             /* Disable to reset FIFO. */
  fifo_cfg.FIFOEn = bTRUE;  
  AD5940_FIFOCfg(&fifo_cfg);                             /* Enable FIFO here */
  
  /* Step3. Interrupt controller */
  
  AD5940_INTCCfg(AFEINTC_1, AFEINTSRC_ALLINT, bTRUE);           /* Enable all interrupt in Interrupt Controller 1, so we can check INTC flags */
  AD5940_INTCCfg(AFEINTC_0, AFEINTSRC_DATAFIFOTHRESH, bTRUE);   /* Interrupt Controller 0 will control GP0 to generate interrupt to MCU */
  AD5940_INTCClrFlag(AFEINTSRC_ALLINT);
  /* Step4: Reconfigure GPIO */
  gpio_cfg.FuncSet = GP6_SYNC|GP5_SYNC|GP4_SYNC|GP2_TRIG|GP1_SYNC|GP0_INT;
  gpio_cfg.InputEnSet = AGPIO_Pin2;
  gpio_cfg.OutputEnSet = AGPIO_Pin0|AGPIO_Pin1|AGPIO_Pin4|AGPIO_Pin5|AGPIO_Pin6;
  gpio_cfg.OutVal = 0;
  gpio_cfg.PullEnSet = 0;

  AD5940_AGPIOCfg(&gpio_cfg);
  AD5940_SleepKeyCtrlS(SLPKEY_UNLOCK);  /* Allow AFE to enter sleep mode. */
  return 0;
}

/* !!Change the application parameters here if you want to change it to none-default value */
void AD5940BIAStructInit(void)
{
  AppBIACfg_Type *pBIACfg;
  
  AppBIAGetCfg(&pBIACfg);
  
  pBIACfg->SeqStartAddr = 0;
  pBIACfg->MaxSeqLen = 512; /** @todo add checker in function */
  
  pBIACfg->RcalVal = 10000.0;
  pBIACfg->DftNum = DFTNUM_16384;
  pBIACfg->NumOfData = -1;      /* Never stop until you stop it mannually by AppBIACtrl() function */
  pBIACfg->BiaODR = 11;         /* ODR(Sample Rate) 20Hz */
  pBIACfg->FifoThresh = 4;      /* 4 */
  pBIACfg->ADCSinc3Osr = ADCSINC3OSR_2;
	pBIACfg->ADCPgaGain = ADCPGA_9;
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
