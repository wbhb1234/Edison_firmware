/**
  ******************************************************************************
  * @file    ppg_sensor.c 
	* @author  ruiyan
	* @date 20180515
  * @version v1.0	
	* @brief   .
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2018 Huami</center></h2>
  *
  ******************************************************************************
  */ 
	
#include "afe4900.h"
#include "ppg_sensor.h"
#include "spi.h"

/* define 3 ppg sensors **************************************************************************************************/
struct afe4900_ppg_sensor_t PPG_sensor_01 = {
	                           .afe_working_stage_x = STAGE_0_CALIBRATE_GAIN,
	                           .sub_stage = 0,
	                           .SPI_write_data_p = AFE01_02_03_SPI_write_data,
                             .SPI_read_data_p = AFE01_02_03_SPI_read_data,	
	                           .set_afe_resetz_pin_high_p = AFE01_set_afe_resetz_pin_high,
                             .set_afe_resetz_pin_low_p = 	AFE01_set_afe_resetz_pin_low,
                             .set_afe_spi_nss_pin_high_p = AFE01_set_afe_spi1_nss_pin_high,	
                             .set_afe_spi_nss_pin_low_p = 	AFE01_set_afe_spi1_nss_pin_low,
                             .disable_afe_interrupt_p = AFE01_disable_afe_interrupt,
                             .enable_afe_interrupt_p = 	AFE01_enable_afe_interrupt,
	                           .amb_channel_ledx = LED_4_AMB_CHANNEL,
	                           .auto_led_current_or_not = AUTO_LED_CURRENT,
};  //afe 01
struct afe4900_ppg_sensor_t PPG_sensor_02 = {
	                           .afe_working_stage_x = STAGE_0_CALIBRATE_GAIN,
	                           .sub_stage = 0,
	                           .SPI_write_data_p = AFE01_02_03_SPI_write_data,
                             .SPI_read_data_p = AFE01_02_03_SPI_read_data,	
	                           .set_afe_resetz_pin_high_p = AFE02_set_afe_resetz_pin_high,
                             .set_afe_resetz_pin_low_p = 	AFE02_set_afe_resetz_pin_low,
                             .set_afe_spi_nss_pin_high_p = AFE02_set_afe_spi1_nss_pin_high,	
                             .set_afe_spi_nss_pin_low_p = 	AFE02_set_afe_spi1_nss_pin_low,
                             .disable_afe_interrupt_p = AFE02_disable_afe_interrupt,
                             .enable_afe_interrupt_p = 	AFE02_enable_afe_interrupt,
		                         .amb_channel_ledx = NO_AMB_CHANNEL,
	                           .auto_led_current_or_not = AUTO_LED_CURRENT,
};  //afe 02
struct afe4900_ppg_sensor_t PPG_sensor_03 = {
	                           .afe_working_stage_x = STAGE_0_CALIBRATE_GAIN,
	                           .sub_stage = 0,
	                           .SPI_write_data_p = AFE01_02_03_SPI_write_data,
                             .SPI_read_data_p = AFE01_02_03_SPI_read_data,	
	                           .set_afe_resetz_pin_high_p = AFE03_set_afe_resetz_pin_high,
                             .set_afe_resetz_pin_low_p = 	AFE03_set_afe_resetz_pin_low,
                             .set_afe_spi_nss_pin_high_p = AFE03_set_afe_spi1_nss_pin_high,	
                             .set_afe_spi_nss_pin_low_p = 	AFE03_set_afe_spi1_nss_pin_low,
                             .disable_afe_interrupt_p = AFE03_disable_afe_interrupt,
                             .enable_afe_interrupt_p = 	AFE03_enable_afe_interrupt,
	                           .amb_channel_ledx = LED_4_AMB_CHANNEL,
	                           .auto_led_current_or_not = AUTO_LED_CURRENT,
};  //afe 03

/* AFE01 02 03 **********************************************************************************************************/
void AFE01_02_03_SPI_write_data(struct afe4900_ppg_sensor_t *sensor_p, uint8_t Reg_address, uint8_t *pData, uint16_t Size)
{
	HAL_StatusTypeDef status;
	sensor_p->set_afe_spi_nss_pin_low_p(sensor_p);
	status = HAL_SPI_Transmit(&hspi1, &Reg_address, 1, 200); //if(status != HAL_OK)return status;
	status = HAL_SPI_Transmit(&hspi1, pData, Size, 200);
	sensor_p->set_afe_spi_nss_pin_high_p(sensor_p);
	return ;
}
void AFE01_02_03_SPI_read_data(struct afe4900_ppg_sensor_t *sensor_p, uint8_t Reg_address,uint8_t *pData, uint16_t Size)
{
	HAL_StatusTypeDef status;
	sensor_p->set_afe_spi_nss_pin_low_p(sensor_p);
	//AFE02_set_afe_spi1_nss_pin_low(sensor_p);
	status = HAL_SPI_Transmit(&hspi1, &Reg_address, 1, 200); //if(status != HAL_OK)return status;
	status = HAL_SPI_Receive(&hspi1, pData, Size, 200);
	//AFE02_set_afe_spi1_nss_pin_high(sensor_p);
	sensor_p->set_afe_spi_nss_pin_high_p(sensor_p);
}

/* AFE01 ****************************************************************************************************************/
void AFE01_set_afe_resetz_pin_high(struct afe4900_ppg_sensor_t *sensor_p)  {HAL_GPIO_WritePin(GPIOC,GPIO_PIN_7,GPIO_PIN_SET);  }
void AFE01_set_afe_resetz_pin_low(struct afe4900_ppg_sensor_t *sensor_p)   {HAL_GPIO_WritePin(GPIOC,GPIO_PIN_7,GPIO_PIN_RESET);}
//void AFE01_set_afe_resetz_pin_high(struct afe4900_ppg_sensor_t *sensor_p)  {HAL_GPIO_WritePin(GPIOG,GPIO_PIN_14,GPIO_PIN_SET);  }
//void AFE01_set_afe_resetz_pin_low(struct afe4900_ppg_sensor_t *sensor_p)   {HAL_GPIO_WritePin(GPIOG,GPIO_PIN_14,GPIO_PIN_RESET);}
void AFE01_set_afe_spi1_nss_pin_high(struct afe4900_ppg_sensor_t *sensor_p){HAL_GPIO_WritePin(GPIOB,GPIO_PIN_2,GPIO_PIN_SET);  }
void AFE01_set_afe_spi1_nss_pin_low(struct afe4900_ppg_sensor_t *sensor_p) {HAL_GPIO_WritePin(GPIOB,GPIO_PIN_2,GPIO_PIN_RESET);}
void AFE01_disable_afe_interrupt(struct afe4900_ppg_sensor_t *sensor_p)    {HAL_NVIC_DisableIRQ(EXTI2_IRQn);                   }
void AFE01_enable_afe_interrupt(struct afe4900_ppg_sensor_t *sensor_p)     {HAL_NVIC_EnableIRQ(EXTI2_IRQn);                    }

/* AFE02 ****************************************************************************************************************/
void AFE02_set_afe_resetz_pin_high(struct afe4900_ppg_sensor_t *sensor_p)  {HAL_GPIO_WritePin(GPIOC,GPIO_PIN_8,GPIO_PIN_SET);  }
void AFE02_set_afe_resetz_pin_low(struct afe4900_ppg_sensor_t *sensor_p)   {HAL_GPIO_WritePin(GPIOC,GPIO_PIN_8,GPIO_PIN_RESET);}
//void AFE02_set_afe_resetz_pin_high(struct afe4900_ppg_sensor_t *sensor_p)  {HAL_GPIO_WritePin(GPIOG,GPIO_PIN_14,GPIO_PIN_SET);  }
//void AFE02_set_afe_resetz_pin_low(struct afe4900_ppg_sensor_t *sensor_p)   {HAL_GPIO_WritePin(GPIOG,GPIO_PIN_14,GPIO_PIN_RESET);}
void AFE02_set_afe_spi1_nss_pin_high(struct afe4900_ppg_sensor_t *sensor_p){HAL_GPIO_WritePin(GPIOB,GPIO_PIN_1,GPIO_PIN_SET);  }
void AFE02_set_afe_spi1_nss_pin_low(struct afe4900_ppg_sensor_t *sensor_p) {HAL_GPIO_WritePin(GPIOB,GPIO_PIN_1,GPIO_PIN_RESET);}
void AFE02_disable_afe_interrupt(struct afe4900_ppg_sensor_t *sensor_p)    {HAL_NVIC_DisableIRQ(EXTI4_IRQn);                   }
void AFE02_enable_afe_interrupt(struct afe4900_ppg_sensor_t *sensor_p)     {HAL_NVIC_EnableIRQ(EXTI4_IRQn);                    }

/* AFE03 ****************************************************************************************************************/
void AFE03_set_afe_resetz_pin_high(struct afe4900_ppg_sensor_t *sensor_p)  {HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_SET);  }
void AFE03_set_afe_resetz_pin_low(struct afe4900_ppg_sensor_t *sensor_p)   {HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_RESET);}
//void AFE03_set_afe_resetz_pin_high(struct afe4900_ppg_sensor_t *sensor_p)  {HAL_GPIO_WritePin(GPIOG,GPIO_PIN_14,GPIO_PIN_SET);  }
//void AFE03_set_afe_resetz_pin_low(struct afe4900_ppg_sensor_t *sensor_p)   {HAL_GPIO_WritePin(GPIOG,GPIO_PIN_14,GPIO_PIN_RESET);}
void AFE03_set_afe_spi1_nss_pin_high(struct afe4900_ppg_sensor_t *sensor_p){HAL_GPIO_WritePin(GPIOB,GPIO_PIN_0,GPIO_PIN_SET);  }
void AFE03_set_afe_spi1_nss_pin_low(struct afe4900_ppg_sensor_t *sensor_p) {HAL_GPIO_WritePin(GPIOB,GPIO_PIN_0,GPIO_PIN_RESET);}
void AFE03_disable_afe_interrupt(struct afe4900_ppg_sensor_t *sensor_p)    {HAL_NVIC_DisableIRQ(EXTI3_IRQn);                   }
void AFE03_enable_afe_interrupt(struct afe4900_ppg_sensor_t *sensor_p)     {HAL_NVIC_EnableIRQ(EXTI3_IRQn);                    }
