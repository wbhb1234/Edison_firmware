/**
  ******************************************************************************
  * @file    uart_print.c 
	* @author  ruiyan
	* @date 20180503
  * @version v1.0	
	* @brief   .
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2018 Huami</center></h2>
  *
  ******************************************************************************
  */ 
#include <stdint.h>
#include "uart_print.h"
#include "usart.h"

static void print_one_hex(uint8_t hex)
{
  uint8_t buf[2] = {'5', '5'};

  for (int8_t i = 1; i >= 0; i--) {
    if ((hex & 0x0f) > 9) {
      buf[i] = 'a' + (hex & 0x0f) - 10;
    } else {
      buf[i] = '0' + (hex & 0x0f);
    }
    hex = hex >> 4;
  }
  HAL_UART_Transmit(&huart1, buf, 2, TIMEOUT_UART_PRINT);  
}

static void print_22bit(uint32_t value)
{
  uint8_t buf[8+3] = {'0'};
  uint8_t len = 0;
 // value &= 0x1fffff; // 22 bits only

  if (value == 0) {
    len = 1;
    goto done;
  } else {
    for (uint32_t i = 1000000000; i > 0; i /= 10) {
      uint8_t tmp = value / i;
      if (tmp != 0) {
        buf[len++] = tmp + '0';
      } else if (len != 0) {
        buf[len++] = '0';
      }
      value -= (tmp * i);
    }
  }
done:
  HAL_UART_Transmit(&huart1, buf, len, TIMEOUT_UART_PRINT);  
}

void print_s22bit(int32_t value)
{
  if (value < 0) {
    uint8_t tmp = '-';
    HAL_UART_Transmit(&huart1, &tmp, 1, TIMEOUT_UART_PRINT);
    value *= -1;
  }
  print_22bit((uint32_t)value);  
}

void print_rn(void)
{
	uint8_t val[2] = {'\r','\n'};
	HAL_UART_Transmit(&huart1, val, 2, TIMEOUT_UART_PRINT);
}

void print_sp(void)
{
	uint8_t val = ' ';
  HAL_UART_Transmit(&huart1, &val, 1, TIMEOUT_UART_PRINT);
}
