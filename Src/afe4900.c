/**
  ******************************************************************************
  * @file    afe4900.c  
	* @author  ruiyan
	* @date 20160816
  * @version v1.0	
	* @brief   afe4900 API file.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 Huami</center></h2>
  *
  ******************************************************************************
  */ 
	
	
#include "afe4900.h"
#include "stdint.h"
#include "stm32l476xx.h"
#include "stm32l4xx_hal.h"

/*******************************************************************************/
void AFE4900_RESETZ_Init (struct afe4900_ppg_sensor_t *sensor_p);
void AFE4900_Enable_HWPDN (struct afe4900_ppg_sensor_t *sensor_p);
void AFE4900_Disable_HWPDN (struct afe4900_ppg_sensor_t *sensor_p);
void AFE4900_Trigger_HWReset (struct afe4900_ppg_sensor_t *sensor_p);
void AFE4900_ADCRDY_Interrupt_Init (struct afe4900_ppg_sensor_t *sensor_p);
void AFE4900_ADCRDY_Interrupt_Disable (struct afe4900_ppg_sensor_t *sensor_p);
signed long AFE4900_Reg_Read(struct afe4900_ppg_sensor_t *sensor_p, unsigned char Reg_address);
void AFE4900_Reg_Write (struct afe4900_ppg_sensor_t *sensor_p, unsigned char reg_address, unsigned long data);
void AFE4900_Enable_Read (struct afe4900_ppg_sensor_t *sensor_p);
void AFE4900_Disable_Read (struct afe4900_ppg_sensor_t *sensor_p);
void delay_us(uint32_t nus);	
extern uint32_t mydata;
void AFE4900_I_LED_DAC_Set(struct afe4900_ppg_sensor_t *sensor_p, double DC_curr[4]);

/**********************************************************************************************************/
/*	External Variables																			          */
/**********************************************************************************************************/
struct reg_map {
  unsigned char addr;
  unsigned long value;
};
static const struct reg_map reg_init_list[] = {
{0x0, 	0x60},//

{0x9, 	0x27/4},              // led2 led on  start 
{0xa, 	0x37/4+0x1F/4},         // led2 led on  end
{0x1, 	0x2b/4},              // led2 sample  start
{0x2, 	0x37/4+0x1F/4},         // led2 sample  end
{0xd, 	0x37/4},              // led2 convert start
{0xe, 	0x37/4+0x1F/4},         // led2 convert end

{0x36, 	0x58/4},              // led3 led on  start
{0x37, 	0x68/4+0x1F/4},         // led3 led on  end
{0x5, 	0x5c/4},              // led3 sample  start
{0x6, 	0x68/4+0x1F/4},         // led3 sample  end
{0xf, 	0x68/4},              // led3 convert start
{0x10, 	0x68/4+0x1F/4},         // led3 convert end

{0x3 , 	0x89/4},              // led1 led on  start
{0x4, 	0x99/4+0x1F/4},         // led1 led on  end
{0x7, 	0x8d/4},              // led1 sample  start
{0x8, 	0x99/4+0x1F/4},         // led1 sample  end
{0x11, 	0x99/4},              // led1 convert start
{0x12, 	0x99/4+0x1F/4},         // led1 convert end

{0x43 , 0xba/4},              // led4 led on  start
{0x44, 	0xca/4+0x1F/4},         // led4 led on  end
{0xb, 	0xbe/4},              // led4 sample  start
{0xc, 	0xca/4+0x1F/4},         // led4 sample  end
{0x13, 	0xca/4},              // led4 convert start
{0x14, 	0xca/4+0x1F/4},         // led4 convert end

{0x52,	0xca/4+0x1F/4+6/4+1},       // data ready interupt start
{0x53,	0xca/4+0x1F/4+6/4+1},       // data ready interupt end

{0x64,	0x0},               // tia power on 
{0x65,	0xca/4+0x1F/4+2/4+1},       // tia power off
{0x66,	0x0},               // adc power on
{0x67,	0xca/4+0x1F/4+2/4+1},       // adc power off
{0x68,	0x0},               // clk power on
{0x69,	0xca/4+0x1F/4+2/4+1},       // clk power off
{0x6a,	0xca/4+0x1F/4+2/4+1+0xB/4+1},   // deep sleep start
{0x6b,	0x04e5/4},            // deep sleep end


{0x1d,	0x4FF/4}, //100Hz
//{0x1d,	0x13FF},//25Hz
{0x1e,	0x003},//num=3
{0x1f,	0x0},
{0x20,	0x0},
//{0x21,	0x000032},//0x000003 TIA_GAIN 50k 0x000002 100k 0x000005 10k 0x003005 10k IFS_OFFDAC +-31.5uA  
{0x21,	0x007015},//TIA_GAIN 0x000002 100k 0x000001 250k 0x000005 10k IFS_OFFDAC +-126uA, 0x007035 25pF
{0x22,	0x000000},//ILED1 0x0C000C-10mA 0x080019-20mA 0x040006-4.9mA 0x000002-1.57mA 0x000013-14.9mA
{0x23,	0x124018},//ILED_2X 0x124218 ILED_1X 0x104218
{0x31, 0x000400},//2.5kHz PD disconnect ENABLE_INPUT_SHORT=0

{0x3D,	0x040000},//watermark FIFO mode
{0x42,	0x1014E0},//fifo=84

{0x4B, 0x000100|0x0}, /*CONTROL4*///{0x4B, 0x000100|0x6}
{0x4E, 0x000000},
{0x50, 0x000008|0x00},
{0x61,	0x00000}, 
{0x62,	0x000000},
{0x72,	0x0000F0}, //2 led driver enabled

{0x4E,  0x000008}, //dual PD enable

{0x45,  0x27/4}, //TG PD1 start
{0x46,  0xca/4+0x1F/4}, //TG PD1 end

{0x47,  0xca/4+0x1F/4+1}, //TG PD2 start
{0x48,  0xca/4+0x1F/4+2}, //TG PD2 end

//{0x6C,	0x000800}, //ENABLE_PD1_SHORT

};

/**********************************************************************************************************/
/*	        AFE4900 Registers Initialization          				                  					  		  */
/**********************************************************************************************************/

void AFE4900_Reg_Init(struct afe4900_ppg_sensor_t *sensor_p)
{
for (uint8_t i = 0; i < sizeof(reg_init_list) / sizeof(reg_init_list[0]); i++) {
		AFE4900_Reg_Write(sensor_p, reg_init_list[i].addr,reg_init_list[i].value);
  }
}


/**********************************************************************************************************/
/*	        AFE4900_RESETZ_Init         				                                          		  */
/**********************************************************************************************************/
void AFE4900_RESETZ_Init (struct afe4900_ppg_sensor_t *sensor_p)
{
	sensor_p->set_afe_resetz_pin_high_p(sensor_p);	  
}


/**********************************************************************************************************/
/*	        AFE4900_Enable_HWPDN         				                                          		  */
/**********************************************************************************************************/
void AFE4900_Enable_HWPDN (struct afe4900_ppg_sensor_t *sensor_p)
{
	sensor_p->set_afe_resetz_pin_low_p(sensor_p);
	HAL_Delay(10);
}

/**********************************************************************************************************/
/*	        AFE4900_Disable_HWPDN         				                                          		  */
/**********************************************************************************************************/
void AFE4900_Disable_HWPDN (struct afe4900_ppg_sensor_t *sensor_p)
{
	sensor_p->set_afe_resetz_pin_high_p(sensor_p);
	HAL_Delay(10);
}

/**********************************************************************************************************/
/*	        AFE4900_Trigger_HWReset         				                                              */
/**********************************************************************************************************/
void AFE4900_Trigger_HWReset (struct afe4900_ppg_sensor_t *sensor_p)
{
	sensor_p->set_afe_resetz_pin_low_p(sensor_p);
	//delay_us(199);                   // ~37us delay  with 32MHz clock
	delay_us(373);                   // ~37us delay  with 32MHz clock
	sensor_p->set_afe_resetz_pin_high_p(sensor_p);
	HAL_Delay(10);
}

/**********************************************************************************************************/
/* AFE4900_ADCRDY_Interrupt_Init												                          */
/**********************************************************************************************************/
void AFE4900_ADCRDY_Interrupt_Init (struct afe4900_ppg_sensor_t *sensor_p)
{
  sensor_p->disable_afe_interrupt_p(sensor_p);
}

/**********************************************************************************************************/
/* AFE4900_ADCRDY_Interrupt_Disable												                          */
/**********************************************************************************************************/
void AFE4900_ADCRDY_Interrupt_Disable (struct afe4900_ppg_sensor_t *sensor_p)
{
	sensor_p->disable_afe_interrupt_p(sensor_p);
}

/*********************************************************************************************************/
/* AFE4900_Reg_Write																	                 */
/*********************************************************************************************************/
void AFE4900_Reg_Write (struct afe4900_ppg_sensor_t *sensor_p, unsigned char reg_address, unsigned long data)
{
  unsigned char configData[3];
  configData[0]=(unsigned char)(data >>16);
  configData[1]=(unsigned char)(((data & 0x00FFFF) >>8));
  configData[2]=(unsigned char)(((data & 0x0000FF)));
	sensor_p->SPI_write_data_p(sensor_p, reg_address, configData, REG_SIZE);
}

/**********************************************************************************************************/
/* AFE4900_Reg_Read           					  			  											  */
/**********************************************************************************************************/
signed long AFE4900_Reg_Read(struct afe4900_ppg_sensor_t *sensor_p, unsigned char Reg_address)
{
  unsigned char configData[3] = {0,0,1};
  signed long retVal;
	sensor_p->SPI_read_data_p(sensor_p, Reg_address,configData, REG_SIZE);
  retVal = configData[0];
  retVal = (retVal << 8) | configData[1];
  retVal = (retVal << 8) | configData[2];
  return retVal;
}

/**********************************************************************************************************/
/*	        AFE4900_Enable_Read        	                  					  							  */
/**********************************************************************************************************/
void AFE4900_Enable_Read (struct afe4900_ppg_sensor_t *sensor_p)
{
  unsigned char configData[3];
  configData[0]=0x00;
  configData[1]=0x00;
  configData[2]=0x61;//
	sensor_p->SPI_write_data_p(sensor_p, CONTROL0, configData, REG_SIZE);
}

/**********************************************************************************************************/
/*	        AFE4900_Disable_Read       	                  					  							  */
/**********************************************************************************************************/
void AFE4900_Disable_Read (struct afe4900_ppg_sensor_t *sensor_p)
{
  unsigned char configData[3];
  configData[0]=0x00;
  configData[1]=0x00;
  configData[2]=0x60;//
	sensor_p->SPI_write_data_p(sensor_p, CONTROL0, configData, REG_SIZE);
}

/**********************************************************************************************************/
/* AFE4900_ADCRDY_Interrupt_Enable												                          */
/**********************************************************************************************************/
void AFE4900_ADCRDY_Interrupt_Enable (struct afe4900_ppg_sensor_t *sensor_p)
{
	sensor_p->enable_afe_interrupt_p(sensor_p);
}

void AFE4900_LEDx_current_set(struct afe4900_ppg_sensor_t *sensor_p, uint16_t I_LEDx, unsigned char led_x, uint8_t type)
{
	  unsigned long data;
	  unsigned char I_code;
	  /*uint8_t I_LED_2314[4];
	  I_LED_2314[0] = sensor_p->sensor_config.LED_2314_current[0];
	  I_LED_2314[1] = sensor_p->sensor_config.LED_2314_current[1];
	  I_LED_2314[2] = sensor_p->sensor_config.LED_2314_current[2];
	  I_LED_2314[3] = sensor_p->sensor_config.LED_2314_current[3];*/
	  
	  if(type == VALUE) 
		{
			if(I_LEDx > 200) I_LEDx = 200;
	    if(I_LEDx < 2)   I_LEDx = 1;	  	
	    I_code=I_LEDx*255/200;
		}
		else if(type == CODE)
		{
			if(I_LEDx > 255) I_LEDx = 255;
	    if(I_LEDx < 1)   I_LEDx = 1;	  	
	    I_code=I_LEDx;
		}
	  
		if(sensor_p->amb_channel_ledx != NO_AMB_CHANNEL)
		{
			if(led_x == sensor_p->amb_channel_ledx) I_code = 0;
		}
		
 	  AFE4900_Enable_Read (sensor_p);
    data = AFE4900_Reg_Read(sensor_p,0x22);
	  AFE4900_Disable_Read (sensor_p);
	  
	  switch(led_x)
		{
			case(1):
			      {data = (data&0xf3ffc0) | ((I_code&0x0000fc)>>2) | ((I_code&0x000003)<<18);  
						sensor_p->sensor_config.LED_2314_current[3-1] = I_code*200/255;
						sensor_p->sensor_config.LED_2314_cur_code[3-1] = I_code;                    } //LED1
			break;
			
			case(2):
			      {data = (data&0xcff03f) | ((I_code&0x0000fc)<<4) | ((I_code&0x000003)<<20);  
						sensor_p->sensor_config.LED_2314_current[1-1] = I_code*200/255;
						sensor_p->sensor_config.LED_2314_cur_code[1-1] = I_code;                    } //LED2
			break;
			
			case(3):
			      {data = (data&0x3c0fff) | ((I_code&0x0000fc)<<10) | ((I_code&0x000003)<<22); 
						sensor_p->sensor_config.LED_2314_current[2-1] = I_code*200/255;
						sensor_p->sensor_config.LED_2314_cur_code[2-1] = I_code;                    } //LED3
			break;
			
			case(4):
			      {AFE4900_Reg_Write (sensor_p, 0x24, (I_code&0x0000ff)<<9);                             
						sensor_p->sensor_config.LED_2314_current[4-1] = I_code*200/255;
						sensor_p->sensor_config.LED_2314_cur_code[4-1] = I_code;                    } //LED4
			break;
			default:break;
		}

	  AFE4900_Reg_Write (sensor_p, 0x22, data);
}
 
void AFE4900_I_LED_DAC_Change(struct afe4900_ppg_sensor_t *sensor_p, double ADC_DC_curr[4])
{
	double DC_curr[4];
	
	DC_curr[0]=ADC_DC_curr[0]-(sensor_p->sensor_config.LED_2314_DAC[0]);
	DC_curr[1]=ADC_DC_curr[1]-(sensor_p->sensor_config.LED_2314_DAC[1]);
	DC_curr[2]=ADC_DC_curr[2]-(sensor_p->sensor_config.LED_2314_DAC[2]);
	DC_curr[3]=ADC_DC_curr[3]-(sensor_p->sensor_config.LED_2314_DAC[3]);
	
  AFE4900_I_LED_DAC_Set(sensor_p, DC_curr);

	sensor_p->sensor_config.LED_2314_DAC[0] = DC_curr[0];
	sensor_p->sensor_config.LED_2314_DAC[1] = DC_curr[1];
	sensor_p->sensor_config.LED_2314_DAC[2] = DC_curr[2];
	sensor_p->sensor_config.LED_2314_DAC[3] = DC_curr[3];	

}

void AFE4900_I_LED_DAC_Set(struct afe4900_ppg_sensor_t *sensor_p, double DC_curr[4])
{
	uint8_t m_offdac_range = 0;
	uint8_t m_code_for_offdac_range = 0;
	uint8_t m_code_for_LED2;
	uint8_t m_code_for_LED3;
	uint8_t m_code_for_LED1;
	uint8_t m_code_for_LED4;
	double max_DC;
	uint8_t conf[][4] = {
		{0x00, 0x00, 0x00, 0x00},
	};		
	
	if(DC_curr[0] > 126) DC_curr[0] = 126;
	if(DC_curr[1] > 126) DC_curr[1] = 126;
	if(DC_curr[2] > 126) DC_curr[2] = 126;
	if(DC_curr[3] > 126) DC_curr[3] = 126;
	
	if(DC_curr[0] < 0) DC_curr[0] = 0;
	if(DC_curr[1] < 0) DC_curr[1] = 0;
	if(DC_curr[2] < 0) DC_curr[2] = 0;
	if(DC_curr[3] < 0) DC_curr[3] = 0;

	max_DC = (DC_curr[0]>DC_curr[1]) ? DC_curr[0] : DC_curr[1];
	max_DC = (max_DC>DC_curr[2]) ? max_DC : DC_curr[2];
	max_DC = (max_DC>DC_curr[3]) ? max_DC : DC_curr[3];
	
	m_offdac_range = (uint8_t) (max_DC / 15.75);  //

	if(m_offdac_range < 1) {
		m_offdac_range = 1;
		m_code_for_offdac_range = 0x0;
	}
	else if(m_offdac_range < 2){
		m_offdac_range = 2;
		m_code_for_offdac_range = 0x3;
	}
	else if(m_offdac_range < 4){
		m_offdac_range = 4;
		m_code_for_offdac_range = 0x5;
	}
	else{
		m_offdac_range = 8;
		m_code_for_offdac_range = 0x7;
	}

	m_code_for_LED2 = (int)((DC_curr[0] / (0.125*m_offdac_range)) + 0.5);
	m_code_for_LED3 = (int)((DC_curr[1] / (0.125*m_offdac_range)) + 0.5);
	m_code_for_LED1 = (int)((DC_curr[2] / (0.125*m_offdac_range)) + 0.5);
	m_code_for_LED4 = (int)((DC_curr[3] / (0.125*m_offdac_range)) + 0.5); // 0.5 is added to round off the float to nearest integer

	AFE4900_Enable_Read (sensor_p);
  sensor_p->SPI_read_data_p(sensor_p,0x3a,&conf[0][1], REG_SIZE);//////////
	AFE4900_Disable_Read (sensor_p);
	conf[0][0] = 0x3a;
	conf[0][1] = (conf[0][1] & 0xE0) | 0x18 | ((m_code_for_LED2 & 0x38) >> 3);
	conf[0][2] = (conf[0][2] & 0x00) | 0x42 | ((m_code_for_LED2 & 0x04) << 5) | ((m_code_for_LED4 & 0x3c)) | ((m_code_for_LED1 & 0x20) >> 5);
	conf[0][3] = (conf[0][3] & 0x00) | 0x10 | ((m_code_for_LED1 & 0x1c) << 3) | ((m_code_for_LED3 & 0x3c) >> 2);
	sensor_p->SPI_write_data_p(sensor_p,conf[0][0],&conf[0][1], REG_SIZE);//////////

	AFE4900_Enable_Read (sensor_p);
  sensor_p->SPI_read_data_p(sensor_p,0x3e,&conf[0][1], REG_SIZE);//////////
	AFE4900_Disable_Read (sensor_p);
	conf[0][0] = 0x3e;
	conf[0][2] = (conf[0][2] & 0xf0) | ((m_code_for_LED2 & 0x01) << 3) | ((m_code_for_LED4 & 0x01) << 2) | ((m_code_for_LED1 & 0x01) << 1) | (m_code_for_LED3 & 0x01);
	conf[0][3] = (conf[0][3] & 0x00) | ((m_code_for_LED2 & 0x40) << 1) | ((m_code_for_LED2 & 0x02) << 5) | ((m_code_for_LED4 & 0x40) >> 1) | ((m_code_for_LED4 & 0x02) << 3);
	conf[0][3] = conf[0][3]          | ((m_code_for_LED1 & 0x40) >> 3) | ((m_code_for_LED1 & 0x02) << 1) | ((m_code_for_LED3 & 0x40) >> 5) | ((m_code_for_LED3 & 0x02) >> 1);
  sensor_p->SPI_write_data_p(sensor_p,conf[0][0],&conf[0][1], REG_SIZE);//////////

	AFE4900_Enable_Read (sensor_p);
  sensor_p->SPI_read_data_p(sensor_p,0x21,&conf[0][1], REG_SIZE);//////////
	AFE4900_Disable_Read (sensor_p);	
	conf[0][0] = 0x21;
	conf[0][2] = (conf[0][2] & 0x8f) | (m_code_for_offdac_range << 4);
	sensor_p->SPI_write_data_p(sensor_p,conf[0][0],&conf[0][1], REG_SIZE);//////////

	DC_curr[0] = -1*m_code_for_LED2*0.125*m_offdac_range;
	DC_curr[1] = -1*m_code_for_LED3*0.125*m_offdac_range;
	DC_curr[2] = -1*m_code_for_LED1*0.125*m_offdac_range;
	DC_curr[3] = -1*m_code_for_LED4*0.125*m_offdac_range;	

}

void set_TIA_Gain(struct afe4900_ppg_sensor_t *sensor_p,enum TIA_GAIN_CODE gain2,enum TIA_GAIN_CODE gain3,enum TIA_GAIN_CODE gain1,enum TIA_GAIN_CODE gain4)
{
	uint8_t conf[][4] = {
		{0x00, 0x00, 0x00, 0x00},
	};
	/*uint8_t gain2 = sensor_p->sensor_config.LED_2314_gain_code[0];
	uint8_t gain3 = sensor_p->sensor_config.LED_2314_gain_code[1];
	uint8_t gain1 = sensor_p->sensor_config.LED_2314_gain_code[2];
	uint8_t gain4 = sensor_p->sensor_config.LED_2314_gain_code[3];*/
		
  AFE4900_Enable_Read (sensor_p); //set ENSEPGAIN4
  sensor_p->SPI_read_data_p(sensor_p,0x23,&conf[0][1], REG_SIZE);
	AFE4900_Disable_Read (sensor_p);	
	conf[0][0] = 0x23;
	conf[0][2] = (conf[0][2] & 0x7F) | 0x80; 
	sensor_p->SPI_write_data_p(sensor_p,conf[0][0],&conf[0][1], REG_SIZE);

	gain2 = (enum TIA_GAIN_CODE) (gain2 & 0xF);
	gain3 = (enum TIA_GAIN_CODE) (gain3 & 0xF);
	gain1 = (enum TIA_GAIN_CODE) (gain1 & 0xF);
	gain4 = (enum TIA_GAIN_CODE) (gain4 & 0xF);
		
	conf[0][0] = 0x20; //TIA_GAIN_SEP = x, TIA_CF_SEP = 10pF-->2.5pF
	conf[0][1] = 0x00;
	conf[0][2] = 0x00;
	conf[0][3] = 0x08 | ((gain2 & 0x8) << 3) | (gain2 & 0x7);
	sensor_p->SPI_write_data_p(sensor_p,conf[0][0],&conf[0][1], REG_SIZE);
	
	conf[0][0] = 0x1F; //TIA_GAIN_SEP2/3 = x, TIA_CF_SEP2/3 = 10pF-->2.5pF
	conf[0][1] = 0x00;
	conf[0][2] = 0x08 | ((gain4 & 0x8) << 3) | (gain4 & 0x7);
	conf[0][3] = 0x08 | ((gain3 & 0x8) << 3) | (gain3 & 0x7);
	sensor_p->SPI_write_data_p(sensor_p,conf[0][0],&conf[0][1], REG_SIZE);
	
	AFE4900_Enable_Read (sensor_p); //TIA_GAIN = x, TIA_CF = 10pF-->2.5pF
  sensor_p->SPI_read_data_p(sensor_p,0x21,&conf[0][1], REG_SIZE);
	AFE4900_Disable_Read (sensor_p);	
	conf[0][0] = 0x21;
	conf[0][3] = 0x08 | ((gain1 & 0x8) << 3) | (gain1 & 0x7);
	sensor_p->SPI_write_data_p(sensor_p,conf[0][0],&conf[0][1], REG_SIZE);		
}

void start_stop_ADC(struct afe4900_ppg_sensor_t *sensor_p, uint8_t start)
{
	uint8_t conf[][4] = {
		{0x00, 0x00, 0x00, 0x00},
	};
	start = start & 0x1;
	
	AFE4900_Enable_Read (sensor_p);
  sensor_p->SPI_read_data_p(sensor_p,0x1E,&conf[0][1], REG_SIZE);//////////
	AFE4900_Disable_Read (sensor_p);
	
	conf[0][0] = 0x1E;
	conf[0][2] = start;
	sensor_p->SPI_write_data_p(sensor_p,conf[0][0],&conf[0][1], REG_SIZE);//////////
}

void reset_timer_counter(struct afe4900_ppg_sensor_t *sensor_p, uint8_t reset)
{
	uint8_t conf[][4] = {
		{0x00, 0x00, 0x00, 0x00},
	};
	reset = (reset & 0x1) < 1;
	
	//AFE4900_Enable_Read (sensor_p);
  //sensor_p->SPI_read_data_p(sensor_p,0x00,&conf[0][1], REG_SIZE);//////////
	//AFE4900_Disable_Read (sensor_p);
	
	conf[0][0] = 0x00;
	conf[0][3] = 0x60 | reset;
	sensor_p->SPI_write_data_p(sensor_p,conf[0][0],&conf[0][1], REG_SIZE);//////////
}

void PD_disconnect(struct afe4900_ppg_sensor_t *sensor_p, uint8_t disconnect)
{
	uint8_t conf[][4] = {
		{0x00, 0x00, 0x00, 0x00},
	};
	disconnect = (disconnect & 0x1) << 2;
	
	AFE4900_Enable_Read (sensor_p);
  sensor_p->SPI_read_data_p(sensor_p,0x31,&conf[0][1], REG_SIZE);
	AFE4900_Disable_Read (sensor_p);
	
	conf[0][0] = 0x31;
	conf[0][2] = disconnect;
	sensor_p->SPI_write_data_p(sensor_p,conf[0][0],&conf[0][1], REG_SIZE);
}

uint16_t tia_gain_code_to_value(enum TIA_GAIN_CODE tia_code)
{
	uint16_t tia_value = 10;
	switch(tia_code)
	{
		case(R_500_K):  tia_value = 500;  break;
		case(R_250_K):  tia_value = 250;  break;
		case(R_100_K):  tia_value = 100;  break;
		case(R_50_K):   tia_value = 50;   break;
		case(R_25_K):   tia_value = 25;   break;
		case(R_10_K):   tia_value = 10;   break;
		case(R_1000_K): tia_value = 1000; break;
		case(R_2000_K): tia_value = 2000; break;
		case(R_1500_K): tia_value = 1500; break;
		default: ;
	}
	return tia_value;
}


void delay_us(uint32_t time)
{       
	do	{
		time--;
	}while(time);
}

void calibrate_gain_calculate_k_b(struct afe4900_ppg_sensor_t *sensor_p)
{
	//y=kx+b
	int32_t x1, x2, y1, y2;
	double mean_k;
	int32_t mean_b;
		
	y2 = sensor_p->sensor_calibration.buf_point[3][0]; y1 = sensor_p->sensor_calibration.buf_point[2][0];
	x2 = sensor_p->sensor_calibration.buf_point[1][0]; x1 = sensor_p->sensor_calibration.buf_point[0][0];
	sensor_p->sensor_calibration.LED_1234_k[0] = (double)(y2-y1)/(x2-x1);
	sensor_p->sensor_calibration.LED_1234_b[0] = y2 - x2*(sensor_p->sensor_calibration.LED_1234_k[0]);
	
	y2 = sensor_p->sensor_calibration.buf_point[3][1]; y1 = sensor_p->sensor_calibration.buf_point[2][1];
	x2 = sensor_p->sensor_calibration.buf_point[1][1]; x1 = sensor_p->sensor_calibration.buf_point[0][1];
	sensor_p->sensor_calibration.LED_1234_k[1] = (double)(y2-y1)/(x2-x1);
	sensor_p->sensor_calibration.LED_1234_b[1] = y2 - x2*(sensor_p->sensor_calibration.LED_1234_k[1]);
	
	y2 = sensor_p->sensor_calibration.buf_point[3][2]; y1 = sensor_p->sensor_calibration.buf_point[2][2];
	x2 = sensor_p->sensor_calibration.buf_point[1][2]; x1 = sensor_p->sensor_calibration.buf_point[0][2];
	sensor_p->sensor_calibration.LED_1234_k[2] = (double)(y2-y1)/(x2-x1);
	sensor_p->sensor_calibration.LED_1234_b[2] = y2 - x2*(sensor_p->sensor_calibration.LED_1234_k[2]);
	
	y2 = sensor_p->sensor_calibration.buf_point[3][3]; y1 = sensor_p->sensor_calibration.buf_point[2][3];
	x2 = sensor_p->sensor_calibration.buf_point[1][3]; x1 = sensor_p->sensor_calibration.buf_point[0][3];
	sensor_p->sensor_calibration.LED_1234_k[3] = (double)(y2-y1)/(x2-x1);
	sensor_p->sensor_calibration.LED_1234_b[3] = y2 - x2*(sensor_p->sensor_calibration.LED_1234_k[3]);	
	
	mean_k = (sensor_p->sensor_calibration.LED_1234_k[0]+sensor_p->sensor_calibration.LED_1234_k[1]+sensor_p->sensor_calibration.LED_1234_k[2]+sensor_p->sensor_calibration.LED_1234_k[3])/4;
	mean_b = (sensor_p->sensor_calibration.LED_1234_b[0]+sensor_p->sensor_calibration.LED_1234_b[1]+sensor_p->sensor_calibration.LED_1234_b[2]+sensor_p->sensor_calibration.LED_1234_b[3])/4;
	
	sensor_p->sensor_calibration.mean_k = mean_k;
	sensor_p->sensor_calibration.mean_b = mean_b;
}

void AFE4900_set_DAC(struct afe4900_ppg_sensor_t *sensor_p, double DAC_curr[4] )
{
	sensor_p->sensor_config.LED_2314_DAC[0] = 0;
	sensor_p->sensor_config.LED_2314_DAC[1] = 0;
	sensor_p->sensor_config.LED_2314_DAC[2] = 0;
	sensor_p->sensor_config.LED_2314_DAC[3] = 0;
	AFE4900_I_LED_DAC_Change(sensor_p, DAC_curr);
}

void calibrate_gain_stage(struct afe4900_ppg_sensor_t *sensor_p)
{
	if(sensor_p->sensor_data.num_of_data_sum > DATA_NUM_FOR_GAIN_CALIBRATION)
	{
		sensor_p->sensor_calibration.buf_point[sensor_p->sub_stage-1][0] = (sensor_p->sensor_data.channel_1234_data_sum[0]) / (sensor_p->sensor_data.num_of_data_sum);
		sensor_p->sensor_calibration.buf_point[sensor_p->sub_stage-1][1] = (sensor_p->sensor_data.channel_1234_data_sum[1]) / (sensor_p->sensor_data.num_of_data_sum);
		sensor_p->sensor_calibration.buf_point[sensor_p->sub_stage-1][2] = (sensor_p->sensor_data.channel_1234_data_sum[2]) / (sensor_p->sensor_data.num_of_data_sum);
		sensor_p->sensor_calibration.buf_point[sensor_p->sub_stage-1][3] = (sensor_p->sensor_data.channel_1234_data_sum[3]) / (sensor_p->sensor_data.num_of_data_sum);
		
		sensor_p->sensor_data.channel_1234_data_sum[0] = 0;  
		sensor_p->sensor_data.channel_1234_data_sum[1] = 0;
		sensor_p->sensor_data.channel_1234_data_sum[2] = 0;
		sensor_p->sensor_data.channel_1234_data_sum[3] = 0;
		
		sensor_p->sensor_data.num_of_data_sum = 0;
		sensor_p->sensor_data.num_of_data_total = 0;
		
		double DAC_curr[4] = {0,0,0,0};
		DAC_curr[0] = 0.125*((sensor_p->sub_stage)&0x1)*tia_gain_code_to_value(R_2000_K)/tia_gain_code_to_value(sensor_p->sensor_config.LED_2314_gain_code[0]);
		DAC_curr[1] = 0.125*((sensor_p->sub_stage)&0x1)*tia_gain_code_to_value(R_2000_K)/tia_gain_code_to_value(sensor_p->sensor_config.LED_2314_gain_code[1]);
		DAC_curr[2] = 0.125*((sensor_p->sub_stage)&0x1)*tia_gain_code_to_value(R_2000_K)/tia_gain_code_to_value(sensor_p->sensor_config.LED_2314_gain_code[2]);
		DAC_curr[3] = 0.125*((sensor_p->sub_stage)&0x1)*tia_gain_code_to_value(R_2000_K)/tia_gain_code_to_value(sensor_p->sensor_config.LED_2314_gain_code[3]);
		
		sensor_p->sensor_config.LED_2314_DAC[0] = 0;
		sensor_p->sensor_config.LED_2314_DAC[1] = 0;
		sensor_p->sensor_config.LED_2314_DAC[2] = 0;
		sensor_p->sensor_config.LED_2314_DAC[3] = 0;
		
		AFE4900_I_LED_DAC_Change(sensor_p,DAC_curr);
		
		switch(sensor_p->sub_stage)
		{
			case(SUB_STAGE_01): {}break;
			case(SUB_STAGE_02): 
      {
				enum TIA_GAIN_CODE gain2 = sensor_p->sensor_config.LED_2314_gain_code[0];
				enum TIA_GAIN_CODE gain3 = sensor_p->sensor_config.LED_2314_gain_code[1];
				enum TIA_GAIN_CODE gain1 = sensor_p->sensor_config.LED_2314_gain_code[2];
				enum TIA_GAIN_CODE gain4 = sensor_p->sensor_config.LED_2314_gain_code[3];
			  set_TIA_Gain(sensor_p,gain2,gain3,gain1,gain4);
			} break;
			case(SUB_STAGE_03): {}break;
			case(SUB_STAGE_04): 
			{
				calibrate_gain_calculate_k_b(sensor_p);             //calculate k and b
				DAC_curr[0] = 0;DAC_curr[1] = 0;
				DAC_curr[2] = 0;DAC_curr[3] = 0;
				AFE4900_set_DAC(sensor_p, DAC_curr);                //set DAC to zero uA
				set_TIA_Gain(sensor_p,R_10_K,R_10_K,R_10_K,R_10_K); //set TIA gain to 10k
				PD_disconnect(sensor_p,CONNECT);
				sensor_p->sub_stage = SUB_STAGE_01;
				sensor_p->afe_working_stage_x = STAGE_1_SET_I_LED;  /* go to STAGE_1_SET_I_LED */
				
				sensor_p->sensor_data.channel_1234_data_sum[0] = 0;  
	      sensor_p->sensor_data.channel_1234_data_sum[1] = 0;
	      sensor_p->sensor_data.channel_1234_data_sum[2] = 0;
	      sensor_p->sensor_data.channel_1234_data_sum[3] = 0;	
	
      	sensor_p->sensor_data.num_of_data_sum = 0;
      	sensor_p->sensor_data.num_of_data_total = 0;
				
				AFE4900_LEDx_current_set(sensor_p, INIT_I_LED, 2, VALUE);	 //
		  	AFE4900_LEDx_current_set(sensor_p, INIT_I_LED, 3, VALUE);
		  	AFE4900_LEDx_current_set(sensor_p, INIT_I_LED, 1, VALUE);	
		  	AFE4900_LEDx_current_set(sensor_p, INIT_I_LED, 4, VALUE); 
			} break;
			default:{}
		}
		sensor_p->sub_stage++;			
	}
}

void set_i_led_stage(struct afe4900_ppg_sensor_t *sensor_p)
{
	if(sensor_p->sensor_data.num_of_data_sum > DATA_NUM_FOR_SET_I_LED)
	{
		double DC_curr[4] = {0,0,0,0};
		DC_curr[0] = (sensor_p->sensor_data.channel_1234_data_sum[0]) / (sensor_p->sensor_data.num_of_data_sum);
		DC_curr[1] = (sensor_p->sensor_data.channel_1234_data_sum[1]) / (sensor_p->sensor_data.num_of_data_sum);
		DC_curr[2] = (sensor_p->sensor_data.channel_1234_data_sum[2]) / (sensor_p->sensor_data.num_of_data_sum);
		DC_curr[3] = (sensor_p->sensor_data.channel_1234_data_sum[3]) / (sensor_p->sensor_data.num_of_data_sum);
		
		DC_curr[0] = (double) ((1.0*DC_curr[0] * 1.2 / (1<<21)) / (2 * tia_gain_code_to_value(R_10_K))) * 1000;
		DC_curr[1] = (double) ((1.0*DC_curr[1] * 1.2 / (1<<21)) / (2 * tia_gain_code_to_value(R_10_K))) * 1000;
		DC_curr[2] = (double) ((1.0*DC_curr[2] * 1.2 / (1<<21)) / (2 * tia_gain_code_to_value(R_10_K))) * 1000;
		DC_curr[3] = (double) ((1.0*DC_curr[3] * 1.2 / (1<<21)) / (2 * tia_gain_code_to_value(R_10_K))) * 1000;
		
		if(sensor_p->auto_led_current_or_not == FIXED_LED_CURRENT) 
		{
			AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_cur_fixed[0], 2, CODE);	 //	
			AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_cur_fixed[1], 3, CODE);
			AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_cur_fixed[2], 1, CODE);	
			AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_cur_fixed[3], 4, CODE); 
			sensor_p->sub_stage = SUB_STAGE_01;
			sensor_p->afe_working_stage_x = STAGE_2_SET_I_OFFDAC;  /* go to STAGE_2_SET_I_OFFDAC */			
		}
		else if(DC_curr[0]>INIT_I_PD+I_PD_ERROR || DC_curr[0]<INIT_I_PD-I_PD_ERROR || 
			      DC_curr[1]>INIT_I_PD+I_PD_ERROR || DC_curr[1]<INIT_I_PD-I_PD_ERROR || 
		        DC_curr[2]>INIT_I_PD+I_PD_ERROR || DC_curr[2]<INIT_I_PD-I_PD_ERROR ||
   		      DC_curr[3]>INIT_I_PD+I_PD_ERROR || DC_curr[3]<INIT_I_PD-I_PD_ERROR  ) 
		{
			AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_current[0]*INIT_I_PD/DC_curr[0], 2, VALUE);	 // make sure  I_PD_RED is 10.5-11.5 uA	
			AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_current[1]*INIT_I_PD/DC_curr[1], 3, VALUE);
			AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_current[2]*INIT_I_PD/DC_curr[2], 1, VALUE);	
			AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_current[3]*INIT_I_PD/DC_curr[3], 4, VALUE); 
      sensor_p->sub_stage++;	 	
		}
		else
		{
			sensor_p->sub_stage = SUB_STAGE_01;
			sensor_p->afe_working_stage_x = STAGE_2_SET_I_OFFDAC;  /* go to STAGE_2_SET_I_OFFDAC */	
		}
		if(sensor_p->sub_stage > SUB_STAGE_MAX)
		{
			sensor_p->sub_stage = SUB_STAGE_01;
			sensor_p->afe_working_stage_x = STAGE_2_SET_I_OFFDAC;  /* go to STAGE_2_SET_I_OFFDAC */
		}
		sensor_p->sensor_data.channel_1234_data_sum[0] = 0;  
		sensor_p->sensor_data.channel_1234_data_sum[1] = 0;
		sensor_p->sensor_data.channel_1234_data_sum[2] = 0;
		sensor_p->sensor_data.channel_1234_data_sum[3] = 0;
		
		sensor_p->sensor_data.num_of_data_sum = 0;
		sensor_p->sensor_data.num_of_data_total = 0;
	}	
}

void set_i_offdac_stage(struct afe4900_ppg_sensor_t *sensor_p)
{
	if(sensor_p->sensor_data.num_of_data_sum > DATA_NUM_FOR_SET_I_OFFDAC)
	{
		double DC_curr[4] = {0,0,0,0};
		DC_curr[0] = (sensor_p->sensor_data.channel_1234_data_sum[0]) / (sensor_p->sensor_data.num_of_data_sum);
		DC_curr[1] = (sensor_p->sensor_data.channel_1234_data_sum[1]) / (sensor_p->sensor_data.num_of_data_sum);
		DC_curr[2] = (sensor_p->sensor_data.channel_1234_data_sum[2]) / (sensor_p->sensor_data.num_of_data_sum);
		DC_curr[3] = (sensor_p->sensor_data.channel_1234_data_sum[3]) / (sensor_p->sensor_data.num_of_data_sum);
		
		DC_curr[0] = (double) ((1.0*DC_curr[0] * 1.2 / (1<<21)) / (2 * tia_gain_code_to_value(R_10_K))) * 1000;
		DC_curr[1] = (double) ((1.0*DC_curr[1] * 1.2 / (1<<21)) / (2 * tia_gain_code_to_value(R_10_K))) * 1000;
		DC_curr[2] = (double) ((1.0*DC_curr[2] * 1.2 / (1<<21)) / (2 * tia_gain_code_to_value(R_10_K))) * 1000;
		DC_curr[3] = (double) ((1.0*DC_curr[3] * 1.2 / (1<<21)) / (2 * tia_gain_code_to_value(R_10_K))) * 1000;				
		
		if(DC_curr[0]>I_DAC_LSB || DC_curr[1]>I_DAC_LSB || DC_curr[2]>I_DAC_LSB || DC_curr[3]>I_DAC_LSB ||
		   DC_curr[0]<-I_DAC_LSB || DC_curr[1]<-I_DAC_LSB || DC_curr[2]<-I_DAC_LSB || DC_curr[3]<-I_DAC_LSB  ) 
		{
		  AFE4900_I_LED_DAC_Change(sensor_p,DC_curr);
      sensor_p->sub_stage++;		
		}
		else
		{
			set_TIA_Gain(sensor_p,R_10_K,R_10_K,R_10_K,R_10_K); //set TIA gain to 10k
			PD_disconnect(sensor_p,DISCONNECT);
			sensor_p->sub_stage = SUB_STAGE_01;
			sensor_p->afe_working_stage_x = STAGE_3_CALIBRATE_I_OFFDAC;  /* go to STAGE_3_CALIBRATE_I_OFFDAC */	
		}
		if(sensor_p->sub_stage > SUB_STAGE_MAX)
		{
			set_TIA_Gain(sensor_p,R_10_K,R_10_K,R_10_K,R_10_K); //set TIA gain to 10k
			PD_disconnect(sensor_p,DISCONNECT);
			sensor_p->sub_stage = SUB_STAGE_01;
			sensor_p->afe_working_stage_x = STAGE_3_CALIBRATE_I_OFFDAC;  /* go to STAGE_3_CALIBRATE_I_OFFDAC */
		}
		sensor_p->sensor_data.channel_1234_data_sum[0] = 0;  
		sensor_p->sensor_data.channel_1234_data_sum[1] = 0;
		sensor_p->sensor_data.channel_1234_data_sum[2] = 0;
		sensor_p->sensor_data.channel_1234_data_sum[3] = 0;
		
		sensor_p->sensor_data.num_of_data_sum = 0;
		sensor_p->sensor_data.num_of_data_total = 0;
	}		
}

void calibrate_i_offdac_stage(struct afe4900_ppg_sensor_t *sensor_p)
{
	if(sensor_p->sensor_data.num_of_data_sum > DATA_NUM_FOR_CALIBRATE_OFFDAC)
	{
		
		if(sensor_p->sub_stage == SUB_STAGE_01)
		{
			sensor_p->sensor_config.LED_2314_DAC_CALI[0] = sensor_p->sensor_data.channel_1234_data_sum[0]/sensor_p->sensor_data.num_of_data_sum;
			sensor_p->sensor_config.LED_2314_DAC_CALI[1] = sensor_p->sensor_data.channel_1234_data_sum[1]/sensor_p->sensor_data.num_of_data_sum;
			sensor_p->sensor_config.LED_2314_DAC_CALI[2] = sensor_p->sensor_data.channel_1234_data_sum[2]/sensor_p->sensor_data.num_of_data_sum;
			sensor_p->sensor_config.LED_2314_DAC_CALI[3] = sensor_p->sensor_data.channel_1234_data_sum[3]/sensor_p->sensor_data.num_of_data_sum;
			
			double DC_curr[4] = {0,0,0,0};
			AFE4900_I_LED_DAC_Set(sensor_p, DC_curr);
			
		  sensor_p->sub_stage = SUB_STAGE_02;                          /* go to SUB_STAGE_02 */
		  sensor_p->afe_working_stage_x = STAGE_3_CALIBRATE_I_OFFDAC;  /* go to STAGE_3_CALIBRATE_I_OFFDAC */	
		}
		else if(sensor_p->sub_stage == SUB_STAGE_02)
		{			
			sensor_p->sensor_config.LED_2314_DAC_CALI[0] -= sensor_p->sensor_data.channel_1234_data_sum[0]/sensor_p->sensor_data.num_of_data_sum;
			sensor_p->sensor_config.LED_2314_DAC_CALI[1] -= sensor_p->sensor_data.channel_1234_data_sum[1]/sensor_p->sensor_data.num_of_data_sum;
			sensor_p->sensor_config.LED_2314_DAC_CALI[2] -= sensor_p->sensor_data.channel_1234_data_sum[2]/sensor_p->sensor_data.num_of_data_sum;
			sensor_p->sensor_config.LED_2314_DAC_CALI[3] -= sensor_p->sensor_data.channel_1234_data_sum[3]/sensor_p->sensor_data.num_of_data_sum;
			
			sensor_p->sensor_config.LED_2314_DAC_CALI[0] *= sensor_p->sensor_calibration.LED_1234_k[0];
			sensor_p->sensor_config.LED_2314_DAC_CALI[1] *= sensor_p->sensor_calibration.LED_1234_k[1];
			sensor_p->sensor_config.LED_2314_DAC_CALI[2] *= sensor_p->sensor_calibration.LED_1234_k[2];
			sensor_p->sensor_config.LED_2314_DAC_CALI[3] *= sensor_p->sensor_calibration.LED_1234_k[3];
			
			enum TIA_GAIN_CODE gain_code[4];
		  gain_code[0] = sensor_p->sensor_config.LED_2314_gain_code[0];
		  gain_code[1] = sensor_p->sensor_config.LED_2314_gain_code[1];
		  gain_code[2] = sensor_p->sensor_config.LED_2314_gain_code[2];
		  gain_code[3] = sensor_p->sensor_config.LED_2314_gain_code[3];
      set_TIA_Gain(sensor_p,gain_code[0],gain_code[1],gain_code[2],gain_code[3]); //
			
			sensor_p->sub_stage = SUB_STAGE_03;                          /* go to SUB_STAGE_03 */
		  sensor_p->afe_working_stage_x = STAGE_3_CALIBRATE_I_OFFDAC;  /* go to STAGE_3_CALIBRATE_I_OFFDAC */	
		}
		else if(sensor_p->sub_stage == SUB_STAGE_03)
		{
			sensor_p->sensor_calibration.LED_1234_b[0] = sensor_p->sensor_data.channel_1234_data_sum[0]/sensor_p->sensor_data.num_of_data_sum;
			sensor_p->sensor_calibration.LED_1234_b[1] = sensor_p->sensor_data.channel_1234_data_sum[1]/sensor_p->sensor_data.num_of_data_sum;
			sensor_p->sensor_calibration.LED_1234_b[2] = sensor_p->sensor_data.channel_1234_data_sum[2]/sensor_p->sensor_data.num_of_data_sum;
			sensor_p->sensor_calibration.LED_1234_b[3] = sensor_p->sensor_data.channel_1234_data_sum[3]/sensor_p->sensor_data.num_of_data_sum;
			
			double ADC_DC_curr[4]={0,0,0,0};
			PD_disconnect(sensor_p,CONNECT);
			AFE4900_I_LED_DAC_Change(sensor_p, ADC_DC_curr);
			
			int32_t buf_curr[4];
			buf_curr[0] = sensor_p->sensor_config.LED_2314_cur_code[0];
			buf_curr[1] = sensor_p->sensor_config.LED_2314_cur_code[1];
			buf_curr[2] = sensor_p->sensor_config.LED_2314_cur_code[2];
			buf_curr[3] = sensor_p->sensor_config.LED_2314_cur_code[3];
			
			//AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_cur_code[0], 2, CODE);
			if(sensor_p->sensor_config.LED_2314_DAC_CALI[0] > -HALF_SCALE) {AFE4900_LEDx_current_set(sensor_p, 0, 2, CODE);}
			if(sensor_p->sensor_config.LED_2314_DAC_CALI[1] > -HALF_SCALE) {AFE4900_LEDx_current_set(sensor_p, 0, 3, CODE);}
			if(sensor_p->sensor_config.LED_2314_DAC_CALI[2] > -HALF_SCALE) {AFE4900_LEDx_current_set(sensor_p, 0, 1, CODE);}
			if(sensor_p->sensor_config.LED_2314_DAC_CALI[3] > -HALF_SCALE) {AFE4900_LEDx_current_set(sensor_p, 0, 4, CODE);}
			
			sensor_p->sensor_config.LED_2314_cur_code[0] = buf_curr[0];
			sensor_p->sensor_config.LED_2314_cur_code[1] = buf_curr[1];
			sensor_p->sensor_config.LED_2314_cur_code[2] = buf_curr[2];
			sensor_p->sensor_config.LED_2314_cur_code[3] = buf_curr[3];
			
			sensor_p->sub_stage = SUB_STAGE_04;                          /* go to SUB_STAGE_04 */
		  sensor_p->afe_working_stage_x = STAGE_3_CALIBRATE_I_OFFDAC;  /* go to STAGE_3_CALIBRATE_I_OFFDAC */	
		}
		else if(sensor_p->sub_stage == SUB_STAGE_04)
		{
			if(sensor_p->sensor_config.LED_2314_DAC_CALI[0] > -HALF_SCALE) 
			{
				sensor_p->sensor_config.LED_2314_DAC_CALI[0] = sensor_p->sensor_data.channel_1234_data_sum[0]/sensor_p->sensor_data.num_of_data_sum;
				sensor_p->sensor_calibration.LED_1234_b[0] = 0;
				AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_cur_code[0], 2, CODE);
			}
			
			if(sensor_p->sensor_config.LED_2314_DAC_CALI[1] > -HALF_SCALE) 
			{
				sensor_p->sensor_config.LED_2314_DAC_CALI[1] = sensor_p->sensor_data.channel_1234_data_sum[1]/sensor_p->sensor_data.num_of_data_sum;
				sensor_p->sensor_calibration.LED_1234_b[1] = 0;
				AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_cur_code[1], 3, CODE);
			}
			
			if(sensor_p->sensor_config.LED_2314_DAC_CALI[2] > -HALF_SCALE) 
			{
				sensor_p->sensor_config.LED_2314_DAC_CALI[2] = sensor_p->sensor_data.channel_1234_data_sum[2]/sensor_p->sensor_data.num_of_data_sum;
				sensor_p->sensor_calibration.LED_1234_b[2] = 0;
				AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_cur_code[2], 1, CODE);
			}
			
			if(sensor_p->sensor_config.LED_2314_DAC_CALI[3] > -HALF_SCALE) 
			{
				sensor_p->sensor_config.LED_2314_DAC_CALI[3] = sensor_p->sensor_data.channel_1234_data_sum[3]/sensor_p->sensor_data.num_of_data_sum;
				sensor_p->sensor_calibration.LED_1234_b[3] = 0;
				AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_cur_code[3], 4, CODE);
			}
			
			double DC_curr[4] = {0,0,0,0};
			AFE4900_I_LED_DAC_Set(sensor_p, DC_curr);
			PD_disconnect(sensor_p,DISCONNECT);
			
			sensor_p->sub_stage = SUB_STAGE_01;                          /* go to SUB_STAGE_01 */
		  sensor_p->afe_working_stage_x = STAGE_4_CALI_ERR_FROME_I_LED_CHANGE;  /* go to STAGE_4_CALI_ERR_FROME_I_LED_CHANGE */	
		}
				
    sensor_p->sensor_data.channel_1234_data_sum[0] = 0;  
		sensor_p->sensor_data.channel_1234_data_sum[1] = 0;
		sensor_p->sensor_data.channel_1234_data_sum[2] = 0;
		sensor_p->sensor_data.channel_1234_data_sum[3] = 0;
		
		sensor_p->sensor_data.num_of_data_sum = 0;
		sensor_p->sensor_data.num_of_data_total = 0;
	}
}

void cali_err_frome_i_led_change_stage(struct afe4900_ppg_sensor_t *sensor_p)
{
	if(sensor_p->sensor_data.num_of_data_sum > DATA_NUM_FOR_CALI_ERR)
	{
		if(sensor_p->sub_stage == SUB_STAGE_01)
		{
			if(sensor_p->sensor_config.LED_2314_DAC_CALI[0] > -HALF_SCALE) {sensor_p->sensor_calibration.LED_1234_b[0] = sensor_p->sensor_data.channel_1234_data_sum[0]/sensor_p->sensor_data.num_of_data_sum;}
		  if(sensor_p->sensor_config.LED_2314_DAC_CALI[1] > -HALF_SCALE) {sensor_p->sensor_calibration.LED_1234_b[1] = sensor_p->sensor_data.channel_1234_data_sum[1]/sensor_p->sensor_data.num_of_data_sum;}
		  if(sensor_p->sensor_config.LED_2314_DAC_CALI[2] > -HALF_SCALE) {sensor_p->sensor_calibration.LED_1234_b[2] = sensor_p->sensor_data.channel_1234_data_sum[2]/sensor_p->sensor_data.num_of_data_sum;}
		  if(sensor_p->sensor_config.LED_2314_DAC_CALI[3] > -HALF_SCALE) {sensor_p->sensor_calibration.LED_1234_b[3] = sensor_p->sensor_data.channel_1234_data_sum[3]/sensor_p->sensor_data.num_of_data_sum;}
				
			int32_t buf_curr[4];
			buf_curr[0] = sensor_p->sensor_config.LED_2314_cur_code[0];
			buf_curr[1] = sensor_p->sensor_config.LED_2314_cur_code[1];
			buf_curr[2] = sensor_p->sensor_config.LED_2314_cur_code[2];
			buf_curr[3] = sensor_p->sensor_config.LED_2314_cur_code[3];
				
			AFE4900_LEDx_current_set(sensor_p, 0, 2, CODE);
			AFE4900_LEDx_current_set(sensor_p, 0, 3, CODE);
			AFE4900_LEDx_current_set(sensor_p, 0, 1, CODE);
			AFE4900_LEDx_current_set(sensor_p, 0, 4, CODE);
				
			sensor_p->sensor_config.LED_2314_cur_code[0] = buf_curr[0];
			sensor_p->sensor_config.LED_2314_cur_code[1] = buf_curr[1];
			sensor_p->sensor_config.LED_2314_cur_code[2] = buf_curr[2];
			sensor_p->sensor_config.LED_2314_cur_code[3] = buf_curr[3];
				
			sensor_p->sub_stage = SUB_STAGE_02;                          /* go to SUB_STAGE_02 */
		  sensor_p->afe_working_stage_x = STAGE_4_CALI_ERR_FROME_I_LED_CHANGE;  /* go to STAGE_4_CALI_ERR_FROME_I_LED_CHANGE */	
		}
		
		else if(sensor_p->sub_stage == SUB_STAGE_02)
		{
			if(sensor_p->sensor_config.LED_2314_DAC_CALI[0] > -HALF_SCALE) {sensor_p->sensor_calibration.LED_1234_b[0] -= sensor_p->sensor_data.channel_1234_data_sum[0]/sensor_p->sensor_data.num_of_data_sum;}
		  if(sensor_p->sensor_config.LED_2314_DAC_CALI[1] > -HALF_SCALE) {sensor_p->sensor_calibration.LED_1234_b[1] -= sensor_p->sensor_data.channel_1234_data_sum[1]/sensor_p->sensor_data.num_of_data_sum;}
		  if(sensor_p->sensor_config.LED_2314_DAC_CALI[2] > -HALF_SCALE) {sensor_p->sensor_calibration.LED_1234_b[2] -= sensor_p->sensor_data.channel_1234_data_sum[2]/sensor_p->sensor_data.num_of_data_sum;}
		  if(sensor_p->sensor_config.LED_2314_DAC_CALI[3] > -HALF_SCALE) {sensor_p->sensor_calibration.LED_1234_b[3] -= sensor_p->sensor_data.channel_1234_data_sum[3]/sensor_p->sensor_data.num_of_data_sum;}
			
			double ADC_DC_curr[4]={0,0,0,0};
			PD_disconnect(sensor_p,CONNECT);
			AFE4900_I_LED_DAC_Change(sensor_p, ADC_DC_curr);
			AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_cur_code[0], 2, CODE);
			AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_cur_code[1], 3, CODE);
			AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_cur_code[2], 1, CODE);
			AFE4900_LEDx_current_set(sensor_p, sensor_p->sensor_config.LED_2314_cur_code[3], 4, CODE);
			
			sensor_p->sub_stage = SUB_STAGE_01;                          /* go to SUB_STAGE_01 */
		  sensor_p->afe_working_stage_x = STAGE_5_POLL_AND_THRESHOLD_DETECT;  /* go to STAGE_5_POLL_AND_THRESHOLD_DETECT */	
			
		}
		
		sensor_p->sensor_data.channel_1234_data_sum[0] = 0;  
		sensor_p->sensor_data.channel_1234_data_sum[1] = 0;
		sensor_p->sensor_data.channel_1234_data_sum[2] = 0;
		sensor_p->sensor_data.channel_1234_data_sum[3] = 0;
		
		sensor_p->sensor_data.num_of_data_sum = 0;
		sensor_p->sensor_data.num_of_data_total = 0;
	}
}

void pull_and_threshold_detect_stage(struct afe4900_ppg_sensor_t *sensor_p)
{
	if(sensor_p->sensor_data.num_of_data_sum > DATA_NUM_FOR_THRESHOLD_DETECT)
	{
		if(sensor_p->sensor_data.channel_1234_data_sum[0]/sensor_p->sensor_data.num_of_data_sum > RESET_THRESHOLD ||
			 sensor_p->sensor_data.channel_1234_data_sum[1]/sensor_p->sensor_data.num_of_data_sum > RESET_THRESHOLD ||
		   sensor_p->sensor_data.channel_1234_data_sum[2]/sensor_p->sensor_data.num_of_data_sum > RESET_THRESHOLD ||
		   sensor_p->sensor_data.channel_1234_data_sum[3]/sensor_p->sensor_data.num_of_data_sum > RESET_THRESHOLD ||
       sensor_p->sensor_data.channel_1234_data_sum[0]/sensor_p->sensor_data.num_of_data_sum < -RESET_THRESHOLD ||
			 sensor_p->sensor_data.channel_1234_data_sum[1]/sensor_p->sensor_data.num_of_data_sum < -RESET_THRESHOLD ||
		   sensor_p->sensor_data.channel_1234_data_sum[2]/sensor_p->sensor_data.num_of_data_sum < -RESET_THRESHOLD ||
		   sensor_p->sensor_data.channel_1234_data_sum[3]/sensor_p->sensor_data.num_of_data_sum < -RESET_THRESHOLD		)
		{
			double DAC_curr[4] = {0,0,0,0};
			AFE4900_set_DAC(sensor_p, DAC_curr);                //set DAC to zero uA
			set_TIA_Gain(sensor_p,R_10_K,R_10_K,R_10_K,R_10_K); //set TIA gain to 10k
			PD_disconnect(sensor_p,CONNECT);	
			sensor_p->sub_stage = SUB_STAGE_01;
			AFE4900_LEDx_current_set(sensor_p, INIT_I_LED, 2, VALUE);	 //
		  AFE4900_LEDx_current_set(sensor_p, INIT_I_LED, 3, VALUE);
		  AFE4900_LEDx_current_set(sensor_p, INIT_I_LED, 1, VALUE);	
		  AFE4900_LEDx_current_set(sensor_p, INIT_I_LED, 4, VALUE); 
		  sensor_p->afe_working_stage_x = STAGE_1_SET_I_LED;  /* go to STAGE_1_SET_I_LED */
		}
		else if(sensor_p->sensor_data.channel_1234_data_sum[0]/sensor_p->sensor_data.num_of_data_sum > CHANGE_OFFDAC_THRESHOLD ||
			      sensor_p->sensor_data.channel_1234_data_sum[1]/sensor_p->sensor_data.num_of_data_sum > CHANGE_OFFDAC_THRESHOLD ||
		        sensor_p->sensor_data.channel_1234_data_sum[2]/sensor_p->sensor_data.num_of_data_sum > CHANGE_OFFDAC_THRESHOLD ||
		        sensor_p->sensor_data.channel_1234_data_sum[3]/sensor_p->sensor_data.num_of_data_sum > CHANGE_OFFDAC_THRESHOLD ||
            sensor_p->sensor_data.channel_1234_data_sum[0]/sensor_p->sensor_data.num_of_data_sum < -CHANGE_OFFDAC_THRESHOLD ||
			      sensor_p->sensor_data.channel_1234_data_sum[1]/sensor_p->sensor_data.num_of_data_sum < -CHANGE_OFFDAC_THRESHOLD ||
		        sensor_p->sensor_data.channel_1234_data_sum[2]/sensor_p->sensor_data.num_of_data_sum < -CHANGE_OFFDAC_THRESHOLD ||
		        sensor_p->sensor_data.channel_1234_data_sum[3]/sensor_p->sensor_data.num_of_data_sum < -CHANGE_OFFDAC_THRESHOLD		)
		{
			uint16_t gain[4];
			gain[0] = tia_gain_code_to_value(sensor_p->sensor_config.LED_2314_gain_code[0]);
			gain[1] = tia_gain_code_to_value(sensor_p->sensor_config.LED_2314_gain_code[1]);
			gain[2] = tia_gain_code_to_value(sensor_p->sensor_config.LED_2314_gain_code[2]);
			gain[3] = tia_gain_code_to_value(sensor_p->sensor_config.LED_2314_gain_code[3]);
			
			double DC_curr[4] = {0,0,0,0};
		  DC_curr[0] = (sensor_p->sensor_data.channel_1234_data_sum[0]) / (sensor_p->sensor_data.num_of_data_sum);
	  	DC_curr[1] = (sensor_p->sensor_data.channel_1234_data_sum[1]) / (sensor_p->sensor_data.num_of_data_sum);
		  DC_curr[2] = (sensor_p->sensor_data.channel_1234_data_sum[2]) / (sensor_p->sensor_data.num_of_data_sum);
		  DC_curr[3] = (sensor_p->sensor_data.channel_1234_data_sum[3]) / (sensor_p->sensor_data.num_of_data_sum);
		
		  DC_curr[0] = (double) ((1.0*DC_curr[0] * 1.2 / (1<<21)) / (2 * gain[0])) * 1000;
		  DC_curr[1] = (double) ((1.0*DC_curr[1] * 1.2 / (1<<21)) / (2 * gain[1])) * 1000;
		  DC_curr[2] = (double) ((1.0*DC_curr[2] * 1.2 / (1<<21)) / (2 * gain[2])) * 1000;
		  DC_curr[3] = (double) ((1.0*DC_curr[3] * 1.2 / (1<<21)) / (2 * gain[3])) * 1000;	
			AFE4900_I_LED_DAC_Change(sensor_p,DC_curr);
			set_TIA_Gain(sensor_p,R_10_K,R_10_K,R_10_K,R_10_K); //set TIA gain to 10k
			PD_disconnect(sensor_p,DISCONNECT);
			sensor_p->sub_stage = SUB_STAGE_01;
		  sensor_p->afe_working_stage_x = STAGE_3_CALIBRATE_I_OFFDAC;  /* go to STAGE_3_CALIBRATE_I_OFFDAC */
		}
		else
		{
			sensor_p->sub_stage = SUB_STAGE_01;
		  sensor_p->afe_working_stage_x = STAGE_5_POLL_AND_THRESHOLD_DETECT;  /* go to STAGE_5_POLL_AND_THRESHOLD_DETECT */
		}
		sensor_p->sensor_data.channel_1234_data_sum[0] = 0;  
		sensor_p->sensor_data.channel_1234_data_sum[1] = 0;
		sensor_p->sensor_data.channel_1234_data_sum[2] = 0;
		sensor_p->sensor_data.channel_1234_data_sum[3] = 0;
		
		sensor_p->sensor_data.num_of_data_sum = 0;
		sensor_p->sensor_data.num_of_data_total = 0;		
	}
}

int16_t afe4900_read_ppg_data(struct afe4900_ppg_sensor_t *sensor_p,int32_t data_buf[32][4])
{
	unsigned char buffer[128*SAMPLE_SIZE];
	signed long dataVal = 0;
	uint8_t ppg_data_size = 0;	
	
	AFE4900_Enable_Read (sensor_p);
  sensor_p->SPI_read_data_p(sensor_p,0x6D,buffer, REG_SIZE);           //read the size of ppg data
	AFE4900_Disable_Read (sensor_p);
	ppg_data_size = (buffer[2]+1)/FIFO_NPHASE*FIFO_NPHASE*SAMPLE_SIZE;   //calculate the size of ppg data -- byte
	
	ppg_data_size = FIFO_READY_SIZE*SAMPLE_SIZE;
	sensor_p->SPI_read_data_p(sensor_p,0xFF,buffer, ppg_data_size);      //read ppg data from FIFO
	//sensor_p->enable_afe_interrupt_p(sensor_p);                          //enable interrupt
	
	for(unsigned char package=0;package<ppg_data_size;package+=FIFO_NPHASE*SAMPLE_SIZE)
	{ 					
		for(uint8_t sample=0;sample<FIFO_NPHASE*SAMPLE_SIZE;sample+=SAMPLE_SIZE)
		{
			dataVal = buffer[package+sample+0];
			dataVal = (dataVal << 8) | buffer[package+sample+1];
			dataVal = (dataVal << 8) | buffer[package+sample+2];
			if((dataVal & 0x00800000) == 0x00800000) dataVal = dataVal | 0xff000000;			
			data_buf[package/(FIFO_NPHASE*SAMPLE_SIZE)][sample/SAMPLE_SIZE] = dataVal - (sensor_p->sensor_config.LED_2314_DAC_CALI[sample/SAMPLE_SIZE]) - (sensor_p->sensor_calibration.LED_1234_b[sample/SAMPLE_SIZE]);		//ppg data = adc_val - DAC_val
			//data_buf[package/(FIFO_NPHASE*SAMPLE_SIZE)][sample/SAMPLE_SIZE] = dataVal;
			if(sensor_p->sensor_data.num_of_data_total > DISCARD_DATA_NUM)
			{
				sensor_p->sensor_data.channel_1234_data_sum[sample/SAMPLE_SIZE] += dataVal;
			}
		}
		if(sensor_p->sensor_data.num_of_data_total > DISCARD_DATA_NUM)
		{
			sensor_p->sensor_data.num_of_data_sum++;
		}
		sensor_p->sensor_data.num_of_data_total++;		
	}
	
	switch(sensor_p->afe_working_stage_x)
	{
		case(STAGE_0_CALIBRATE_GAIN):             {calibrate_gain_stage(sensor_p);}              break;		
		case(STAGE_1_SET_I_LED):                  {set_i_led_stage(sensor_p);}                   break;
		case(STAGE_2_SET_I_OFFDAC):               {set_i_offdac_stage(sensor_p);}                break;
		case(STAGE_3_CALIBRATE_I_OFFDAC):         {calibrate_i_offdac_stage(sensor_p);}          break;
		case(STAGE_4_CALI_ERR_FROME_I_LED_CHANGE):{cali_err_frome_i_led_change_stage(sensor_p);} break;
		case(STAGE_5_POLL_AND_THRESHOLD_DETECT):  {pull_and_threshold_detect_stage(sensor_p);}   break;
		default:;
	}
	return ppg_data_size/SAMPLE_SIZE/FIFO_NPHASE;
}

/**********************************************************************************************************/
/*  AFE4900_config_init											                          */
/**********************************************************************************************************/
void AFE4900_config_init(struct afe4900_ppg_sensor_t *sensor_p)
{
  AFE4900_RESETZ_Init (sensor_p);
  AFE4900_Enable_HWPDN (sensor_p);
  AFE4900_Disable_HWPDN (sensor_p);
  AFE4900_Trigger_HWReset (sensor_p);
  AFE4900_ADCRDY_Interrupt_Init(sensor_p);
  AFE4900_Reg_Init(sensor_p);	
	reset_timer_counter(sensor_p, RESET_TIMER);
	sensor_p->disable_afe_interrupt_p(sensor_p);
}

/**********************************************************************************************************/
/*	        AFE4900_Init         				                                          				  */
/**********************************************************************************************************/
void AFE4900_Init(struct afe4900_ppg_sensor_t *sensor_p)
{
  AFE4900_config_init(sensor_p); //init afe4900 config
	
	sensor_p->sensor_config.LED_2314_gain_code[0] = INIT_TIA_GAIN; //save TIA gain value
	sensor_p->sensor_config.LED_2314_gain_code[1] = INIT_TIA_GAIN;
	sensor_p->sensor_config.LED_2314_gain_code[2] = INIT_TIA_GAIN;
	sensor_p->sensor_config.LED_2314_gain_code[3] = INIT_TIA_GAIN;
	
	AFE4900_LEDx_current_set(sensor_p, 0, 2, VALUE);	 //
	AFE4900_LEDx_current_set(sensor_p, 0, 3, VALUE);
	AFE4900_LEDx_current_set(sensor_p, 0, 1, VALUE);	
	AFE4900_LEDx_current_set(sensor_p, 0, 4, VALUE);		
	
	PD_disconnect(sensor_p,DISCONNECT);
	double DAC_curr[4] = {0,0,0,0};
	
	/*AFE4900_set_DAC() must first, than set_TIA_Gain()!!!!!*/
	AFE4900_set_DAC(sensor_p, DAC_curr);
	set_TIA_Gain(sensor_p,R_10_K,R_10_K,R_10_K,R_10_K);	 //prepare foe STAGE_0_CALIBRATE_GAIN
	
	sensor_p->sensor_data.channel_1234_data_sum[0] = 0;  
	sensor_p->sensor_data.channel_1234_data_sum[1] = 0;
	sensor_p->sensor_data.channel_1234_data_sum[2] = 0;
	sensor_p->sensor_data.channel_1234_data_sum[3] = 0;	
	
	sensor_p->sensor_data.num_of_data_sum = 0;
	sensor_p->sensor_data.num_of_data_total = 0;
	
	sensor_p->afe_working_stage_x = STAGE_0_CALIBRATE_GAIN; //go to STAGE_0_CALIBRATE_GAIN
	sensor_p->sub_stage = SUB_STAGE_01;
	
	reset_timer_counter(sensor_p, SET_TIMER);	
	//set_TIA_Gain(sensor_p,R_10_K,R_10_K,R_10_K,R_10_K);	 //prepare foe STAGE_0_CALIBRATE_GAIN	
	//start_stop_ADC(sensor_p, START_ADC);
}

// End of file
