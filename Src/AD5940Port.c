/*!
*****************************************************************************
@file:    NUCLEOL476Port.c
@author:  $Author: ADI $
@brief:   The port for ST's Nucleo L452 board.
@version: $Revision: 1 $
@date:    $Date: 2018-04-16 14:09:35 +0100 (Mon, 16 Apr 2018) $
-----------------------------------------------------------------------------
Copyright (c) 2012-2017 Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Modified versions of the software must be conspicuously marked as such.
  - This software is licensed solely and exclusively for use with processors
    manufacTRUEd by or for Analog Devices, Inc.
  - This software may not be combined or merged with other code in any manner
    that would cause the software to become subject to terms and conditions
    which differ from those listed here.
  - Neither the name of Analog Devices, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
  - The use of this software may or may not infringe the patent rights of one
    or more patent holders.  This license does not release you from the
    requirement that you obtain separate licenses from these patent holders
    to use this software.
THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR ECGLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-
INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL ANALOG DEVICES, INC. OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF
CLAIMS OF INTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

#include <AD5940.h>
#include "stdio.h"
#include "stm32l4xx_hal.h"
#include "spi.h"

/* Definition for STM32 SPI clock resources */
#define AD5940SPI                          SPI2
#define AD5940_CLK_ENABLE()                __HAL_RCC_SPI2_CLK_ENABLE()
#define AD5940_SCK_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOB_CLK_ENABLE()
#define AD5940_MISO_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOB_CLK_ENABLE()
#define AD5940_MOSI_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOB_CLK_ENABLE()

#define AD5940SPI_FORCE_RESET()               __HAL_RCC_SPI2_FORCE_RESET()
#define AD5940SPI_RELEASE_RESET()             __HAL_RCC_SPI2_RELEASE_RESET()

/* Definition for AD5940_01 Pins */
#define AD5940_01_CS_GPIO_CLK_ENABLE()        __HAL_RCC_GPIOB_CLK_ENABLE()
#define AD5940_01_RST_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOB_CLK_ENABLE()
#define AD5940_01_GP0INT_GPIO_CLK_ENABLE()    __HAL_RCC_GPIOB_CLK_ENABLE()

#define AD5940_01_SCK_PIN                     GPIO_PIN_13
#define AD5940_01_SCK_GPIO_PORT               GPIOB
#define AD5940_01_SCK_AF                      GPIO_AF5_SPI2
#define AD5940_01_MISO_PIN                    GPIO_PIN_14
#define AD5940_01_MISO_GPIO_PORT              GPIOB
#define AD5940_01_MISO_AF                     GPIO_AF5_SPI2
#define AD5940_01_MOSI_PIN                    GPIO_PIN_15
#define AD5940_01_MOSI_GPIO_PORT              GPIOB
#define AD5940_01_MOSI_AF                     GPIO_AF5_SPI2

#define AD5940_01_CS_PIN                      GPIO_PIN_5   //ICG1
#define AD5940_01_CS_GPIO_PORT                GPIOB

#define AD5940_01_RST_PIN                     GPIO_PIN_9   //ICG1
#define AD5940_01_RST_GPIO_PORT               GPIOB

#define AD5940_01_GP0INT_PIN                  GPIO_PIN_8  // ICG1  
#define AD5940_01_GP0INT_GPIO_PORT            GPIOB
#define AD5940_01_GP0INT_IRQn                 EXTI9_5_IRQn

/* Definition for AD5940_02 Pins */
#define AD5940_02_CS_GPIO_CLK_ENABLE()        __HAL_RCC_GPIOB_CLK_ENABLE()
#define AD5940_02_RST_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOC_CLK_ENABLE()
#define AD5940_02_GP0INT_GPIO_CLK_ENABLE()    __HAL_RCC_GPIOC_CLK_ENABLE()

#define AD5940_02_SCK_PIN                     GPIO_PIN_13
#define AD5940_02_SCK_GPIO_PORT               GPIOB
#define AD5940_02_SCK_AF                      GPIO_AF5_SPI2
#define AD5940_02_MISO_PIN                    GPIO_PIN_14
#define AD5940_02_MISO_GPIO_PORT              GPIOB
#define AD5940_02_MISO_AF                     GPIO_AF5_SPI2
#define AD5940_02_MOSI_PIN                    GPIO_PIN_15
#define AD5940_02_MOSI_GPIO_PORT              GPIOB
#define AD5940_02_MOSI_AF                     GPIO_AF5_SPI2

#define AD5940_02_CS_PIN                      GPIO_PIN_12   //ICG2
#define AD5940_02_CS_GPIO_PORT                GPIOB

#define AD5940_02_RST_PIN                     GPIO_PIN_13   //ICG2
#define AD5940_02_RST_GPIO_PORT               GPIOC

#define AD5940_02_GP0INT_PIN                  GPIO_PIN_9  // ICG2  
#define AD5940_02_GP0INT_GPIO_PORT            GPIOC
#define AD5940_02_GP0INT_IRQn                 EXTI9_5_IRQn

struct ad5940_icg_sensor_t ICG_sensor_01 = {
																						.SPI_CsClr_p = AD5940_01_CsClr,
																						.SPI_CsSet_p = AD5940_01_CsSet,
																						.set_sensor_RstClr_p = AD5940_01_RstClr,
																						.set_sensor_RstSet_p = AD5940_01_RstSet,
																						.enable_sensor_interrupt_p = AD5940_01_Enable_Interrupt,
																						.disable_sensor_interrupt_p = AD5940_01_Disable_Interrupt,
																						.get_sensor_intflag_p = AD5940_01_GetMCUIntFlag,
																						.clr_sensor_intflag_p = AD5940_01_ClrMCUIntFlag,
};	

struct ad5940_icg_sensor_t ICG_sensor_02 = {
																						.SPI_CsClr_p = AD5940_02_CsClr,
																						.SPI_CsSet_p = AD5940_02_CsSet,
																						.set_sensor_RstClr_p = AD5940_02_RstClr,
																						.set_sensor_RstSet_p = AD5940_02_RstSet,
																						.enable_sensor_interrupt_p = AD5940_02_Enable_Interrupt,
																						.disable_sensor_interrupt_p = AD5940_02_Disable_Interrupt,
																						.get_sensor_intflag_p = AD5940_02_GetMCUIntFlag,
																						.clr_sensor_intflag_p = AD5940_02_ClrMCUIntFlag,
};

#define SYSTICK_MAXCOUNT ((1L<<24)-1)   /* we use Systick to complete function Delay10uS(). This value only applies to NUCLEOL452 board. */
#define SYSTICK_CLKFREQ   80000000L     /* Systick clock frequency in Hz. This only appies to NUCLEOL452 board */

SPI_HandleTypeDef  SpiHandle;

volatile static uint32_t TmrIntCount = 0; /* Used for timer */
extern  uint8_t icg01_ucInterrupted;
extern  uint8_t icg02_ucInterrupted;

void AD5940_Delay10us(uint32_t time)
{
	
  if(time==0)return;
  if(time*10<SYSTICK_MAXCOUNT/(SYSTICK_CLKFREQ/1000000)){
    SysTick->LOAD = time*10*(SYSTICK_CLKFREQ/1000000);
    SysTick->VAL = 0;
    SysTick->CTRL = (1 << 2) | (1<<0);    
    while(((SysTick->CTRL)&(1<<16))==0);    
    SysTick->CTRL = 0;                    
  }
  else {
    AD5940_Delay10us(time/2);
    AD5940_Delay10us(time/2 + (time&1));
  }
}
void AD5940_ReadWriteNBytes(unsigned char *pSendBuffer,unsigned char *pRecvBuff,unsigned long length)
{
  HAL_SPI_TransmitReceive(&hspi2, pSendBuffer, pRecvBuff, length, (uint32_t)-1);
}

/* AD5940 - 01 ########################################################################################################################*/
void AD5940_01_CsClr(struct ad5940_icg_sensor_t *sensor_p) 	{ HAL_GPIO_WritePin(AD5940_01_CS_GPIO_PORT, AD5940_01_CS_PIN, GPIO_PIN_RESET);}
void AD5940_01_CsSet(struct ad5940_icg_sensor_t *sensor_p) 	{ HAL_GPIO_WritePin(AD5940_01_CS_GPIO_PORT, AD5940_01_CS_PIN, GPIO_PIN_SET);}
void AD5940_01_RstClr(struct ad5940_icg_sensor_t *sensor_p) 	{ HAL_GPIO_WritePin(AD5940_01_RST_GPIO_PORT, AD5940_01_RST_PIN, GPIO_PIN_SET);}
void AD5940_01_RstSet(struct ad5940_icg_sensor_t *sensor_p) 	{ HAL_GPIO_WritePin(AD5940_01_RST_GPIO_PORT, AD5940_01_RST_PIN, GPIO_PIN_RESET);}
void AD5940_01_Enable_Interrupt(struct ad5940_icg_sensor_t *sensor_p) 	{ HAL_NVIC_EnableIRQ(AD5940_01_GP0INT_IRQn);}
void AD5940_01_Disable_Interrupt(struct ad5940_icg_sensor_t *sensor_p) 	{ HAL_NVIC_DisableIRQ(AD5940_01_GP0INT_IRQn);}
uint32_t AD5940_01_GetMCUIntFlag(struct ad5940_icg_sensor_t *sensor_p) 	{ return icg01_ucInterrupted;}
uint32_t AD5940_01_ClrMCUIntFlag(struct ad5940_icg_sensor_t *sensor_p) 	{ icg01_ucInterrupted = 0;  return 1;}

/* AD5940 - 02 ########################################################################################################################*/
void AD5940_02_CsClr(struct ad5940_icg_sensor_t *sensor_p) 	{ HAL_GPIO_WritePin(AD5940_02_CS_GPIO_PORT, AD5940_02_CS_PIN, GPIO_PIN_RESET);}
void AD5940_02_CsSet(struct ad5940_icg_sensor_t *sensor_p) 	{ HAL_GPIO_WritePin(AD5940_02_CS_GPIO_PORT, AD5940_02_CS_PIN, GPIO_PIN_SET);}
void AD5940_02_RstClr(struct ad5940_icg_sensor_t *sensor_p) 	{ HAL_GPIO_WritePin(AD5940_02_RST_GPIO_PORT, AD5940_02_RST_PIN, GPIO_PIN_SET);}
void AD5940_02_RstSet(struct ad5940_icg_sensor_t *sensor_p) 	{ HAL_GPIO_WritePin(AD5940_02_RST_GPIO_PORT, AD5940_02_RST_PIN, GPIO_PIN_RESET);}
void AD5940_02_Enable_Interrupt(struct ad5940_icg_sensor_t *sensor_p) 	{ HAL_NVIC_EnableIRQ(AD5940_02_GP0INT_IRQn);}
void AD5940_02_Disable_Interrupt(struct ad5940_icg_sensor_t *sensor_p) 	{ HAL_NVIC_DisableIRQ(AD5940_02_GP0INT_IRQn);}
uint32_t AD5940_02_GetMCUIntFlag(struct ad5940_icg_sensor_t *sensor_p) 	{ return icg02_ucInterrupted;}
uint32_t AD5940_02_ClrMCUIntFlag(struct ad5940_icg_sensor_t *sensor_p) 	{ icg02_ucInterrupted = 0;  return 1;}


uint32_t AD5940_MCUResourceInit(void *pCfg)
{
	return 0;
}
